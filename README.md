Image Tagger
=============

## Initialize

Refer bootstrap's css file in `head` tag. Then import `image-tagger-vendor.js` and `image-tagger-bundle.js`, the former packs all dependencies.
After all of that, call `imageTaggerSetup` to initialize tagger:

```javascript
imageTaggerSetup(
  'image-tagger-wrapper', // container's id
  'http://192.168.1.183:7334/image', // api url
  0, // taskId
  0x1f // control tools select, default 0x1f
);
```

## Data format

Tagger `GET` the api url with task id and sequence id to get image data. Server should return a JSON doc.

`optype` can be `"tag"` or `"classify"`.
`type` of attribute *must* more than 0 and less than 10.

```json
{
  "errcode":0,
  "unitid":3267940,
  "imgid":"11425736066558964286",
  "seqid":2,
  "maxseqid":50,
  "url":"http://211.157.161.43/img005/286/9e905e6a989db23e.jpg",
  "description":"",
  "src":"",
  "optype":"tag",
  "sequence":{
    "seqid":3,
    "maxseqid":3,
    "images":[
      {
        "width":750,
        "height":500,
        "imgid":"10033388574809970948",
        "url":"http://pic.adkalava.com/img005/687/8e440a7111973427.jpg",
        "objs":[
          {
            "name":"连衣裙",
            "bndBoxes":[
              {
                "ymax":0.87931727757351,
                "attrs":[
                  {
                    "name":"廓形_新",
                    "values":[
                      {
                        "name":"A型",
                        "uid":7647,

                      }
                    ],
                    "uid":2733
                  }
                ],
                "xmin":0.3707,
                "ymin":0.316,
                "uid":3772062,
                "xmax":0.6134
              }
            ],
            "uid":2
          }
        ]
      }
    ]
  },
  "objs":[
    {
      "name":"连衣裙",
      "uid":42341,
      "geometry":"bndbox",
      "bndBoxes":[
        {
          "editable":false,
          "uid":70,
          "xmin":0.2191726525351,
          "ymin":0.12427184466019,
          "xmax":0.90455938400231,
          "ymax":0.59902912621359,
          "stroke":"black"
        }
      ],
      "attrs":[
        {
          "uid":0,
          "unique":true,
          "required":true,
          "type":2,
          "display":true,
          "name":"裙型",
          "options":[
            {
              "name":"Option 0",
              "uid":0
            }
          ]
        },
        {
          "uid":1,
          "name":"颜色",
          "options":[
            {
              "name":"红",
              "uid":0
            },
            {
              "name":"黄",
              "uid":1
            },
            {
              "name":"蓝",
              "uid":2
            }
          ]
        }
      ]
    }
  ]
}
```

Tagger send post request to save user's tagging every time the submit button is clicked.

NOTE: front end script abandon old sequence whenever new sequence is seeked. It depends on the server to save all the changes.

The sequence id and task id is encoded in the post url. The POST body lookes like the data provide by the server except few changes. Response to `POST` request is expected to contain `maxseqid`.

`operation` can be any of `"skip"`, `"alter"` or `"seq-index"`. 

`"seq-index"` is used to index by sequence. When a sequence indicate button is clicked, the frontend post the following data to the serser:
```json
{
  "unitid": 3267940,
  "url": "http://211.157.161.43/img002/51/b5b89bb7932845e3.jpg",
  "imgid": "11425736066558964286",
  "seqid": 2,
  "operation": "seq-index"
}
```
And it expect the server to response a image data *with that seqid field*, which is essential.

```json
{
  "unitid": 3267940,
  "url": "http://211.157.161.43/img002/51/b5b89bb7932845e3.jpg",
  "imgid": "11425736066558964286",
  "operation": "alter",
  "objs": [
    {
      "name": "连衣裙",
      "uid": 42341,
      "geometry": "bndbox",
      "bndBoxes": [
        {
          "xmin": 63.234356194361595,
          "ymin": 193.39999389648438,
          "xmax": 355.776550669111,
          "ymax": 478.3999938964844,
          "attrs": [
            {
              "uid": 0,
              "name": "裙型",
              "values": [
                {
                  "name": "C",
                  "uid": 2
                },
                {
                  "name": "E",
                  "uid": 4
                }
              ]
            },
            {
              "uid": 1,
              "name": "颜色",
              "values": [
                {
                  "name": "红",
                  "uid": 0
                }
              ]
            }
          ],
          "stroke": "#DC143C"
        }
      ]
    },
    {
      "name": "新裙子",
      "uid": 42342,
      "geometry": "bndbox",
      "bndBoxes": []
    }
  ]
}
```
