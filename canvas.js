import React from 'react';
import { fabric } from 'fabric';
import { CropRect } from './crop-mode.js';
import ClickNDragCrop from './drag-crop-mode.js';
import ClickCrop from './click-crop-mode.js';

var unsupportAlert = "您的浏览器不支持画布";
fabric.Object.prototype.cornerSize = 6;
fabric.Object.prototype.borderColor = '#505050';
fabric.Object.prototype.transparentCorners = false;
fabric.Object.prototype.lockRotation = true;
fabric.Object.prototype.hasRotatingPoint = false;

class Canvas extends React.Component {
  constructor(props) {
    super(props);
    // `props.onCropSelected`
    // `props.onPageInc`
    this._canvas = null;
    this._containerElem = null;
    this._currentMode = null;
  }
  get cropMode() {
    return this._cropMode;
  }
  changeMode(mode) {
    if (this._currentMode === mode)
      return;
    var rects = null;
    var name = null;
    if (this._cropMode) {
      name = this._cropMode.name || '';
      rects = this._cropMode.getRects();
      this._cropMode.cleanup();
    }
    switch (mode) {
      case 'click':
        this._cropMode = new ClickCrop(this._canvas);
        this._cropMode.name = name
        this._currentMode = 'click';
        break;
      case 'drag':
        this._cropMode = new ClickNDragCrop(this._canvas);
        this._cropMode.name = name
        this._currentMode = 'drag';        
        break;
    }
    this._cropMode.addCrops(rects, ['attrs']);
    this._cropMode.onCropSelected = this.props.onCropSelected;
    this._cropMode.tagMode();
  }
  _handlePageInc(step) {
    if (this.props.onPageInc)
      this.props.onPageInc(step);
  }
  componentDidMount() {
    var container = document.getElementById('image-tagger-picture-container');
    this._containerElem = container;
    this._canvas = new fabric.Canvas('image-tagger-canvas', {
      height: container.clientHeight,
      width: container.clientWidth
    });
    this._cropMode = new ClickNDragCrop(this._canvas);
    this._cropMode.tagMode();
    this._cropMode.onCropSelected = this.props.onCropSelected;
  }
  render() {
    return (
      <div id="image-tagger-picture-container" style={ this.props.style }>
        <canvas id="image-tagger-canvas">{ unsupportAlert }</canvas>
      </div>
    );
  }
}

Canvas.propTypes = {
  style: React.PropTypes.object
};

export { Canvas as default };
