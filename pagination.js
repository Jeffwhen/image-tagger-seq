import React from 'react';
import ReactDOM from 'react-dom';
import * as Bootstrap from 'react-bootstrap';

class Pagination extends React.Component {
  constructor(props) {
    super(props);
    // `props.onChange` get called with eventKey when instance changes.
    // `props.onSubmit` get called when submit button is clicked.
    // `props.beforeChange` get called before page change, if return `false`
    // the change is abandoned.
    // `props.onIndicatorClicked` get called if a indicator is clicked, the
    // outside handler should increase the page number manually.
    this.state = {
      length: 0,
      activePage: 0
    };
    this._handleSelect = this._handleSelect.bind(this);
    this._handlePageInput = this._handlePageInput.bind(this);
    this._handlePageInputClick = this._handlePageInputClick.bind(this);
    this.incKey = this.incKey.bind(this);
  }
  // Handle next, previous click.
  // Return -1 if first page reached, 1 if last page reached.
  // Return 0 otherwise.
  incKey(step) {
    step = step || 1;
    var eventKey = this.state.activePage + step;
    return this._handleSelect(eventKey);
  }
  _handleIndicatorClicked(step) {
    if (this.props.onIndicatorClicked) {
      this.props.onIndicatorClicked(step);
    } else {
      this.incKey(step);
    }
  }
  get activePage() {
    return this.state.activePage;
  }
  set activePage(activePage) {
    if (activePage < 1 || activePage > this.state.length) {
      console.log('ImageTaggerWarning: Pagination activePage overflows.');
      return;
    }
    this.state.activePage = activePage;
    ReactDOM.findDOMNode(this.refs.pageInput).value = activePage;
    this.forceUpdate();
  }
  get length() {
    return this.state.length;
  }
  set length(length) {
    if (length < 1) {
      console.error('ImageTaggerError: set Pagination',
                    'length to a value less than 1.');
      throw new Error('Bad argument!');
    }
    this.state.length = length;
    this.forceUpdate();
  }
  // All activePage state changes end up here.
  // We call props.onChange here
  // Return -1 if first page reached, 1 if last page reached.
  // Return 2 if operation is abandoned.
  // Return 0 otherwise.
  _handleSelect(eventKey) {
    var rtv = 0;
    if (eventKey < 1) {
      console.log('ImageTaggerWarning: too small page number!');
      eventKey = 1;
      rtv = -1;
    }
    if (eventKey > this.state.length) {
      console.log('ImageTaggerWarning: too large page number!');
      eventKey = this.state.length;
      rtv = 1;
    }
    if (this.state.activePage === eventKey) return rtv;
    if (!this.props.beforeChange()) {
      rtv = 2;
      return rtv;
    }
    this.state.activePage = eventKey;
    ReactDOM.findDOMNode(this.refs.pageInput).value = eventKey;
    this.forceUpdate();
    if (typeof this.props.onChange === 'function') {
      this.refs.submitButton.disabled = true;
      this.refs.nextButton.disabled = true;
      this.refs.previousButton.disabled = true;
      this.props.onChange(eventKey, () => {
        this.refs.submitButton.disabled = false;
        this.refs.nextButton.disabled = this.state.activePage === this.state.length;
        this.refs.previousButton.disabled = this.state.activePage === 1;
      });
    }
    return rtv;
  }
  _handlePageInputClick(event) {
    event.target.select();
  }
  _handlePageInput(event) {
    event.preventDefault();
    var eventKey = event.target.activePage.value;
    eventKey = Math.floor(Number(eventKey));
    if (Number.isNaN(eventKey)) {
      console.log('ImageTaggerWarning: Bad input!');
      return;
    }
    this._handleSelect(eventKey);
  }
  _handleSkip(event) {
    event.preventDefault();
    if (this.props.onSubmit) {
      this.refs.submitButton.disabled = true;
      this.refs.nextButton.disabled = true;
      this.refs.previousButton.disabled = true;
      this.props.onSkip(() => {
        this.refs.submitButton.disabled = false;
        this.refs.nextButton.disabled = this.state.activePage === this.state.length;
        this.refs.previousButton.disabled = this.state.activePage === 1;
      });
    }
  }
  _handleSubmit(event) {
    event.preventDefault();
    if (this.props.onSubmit) {
      this.refs.submitButton.disabled = true;
      this.refs.nextButton.disabled = true;
      this.refs.previousButton.disabled = true;
      this.props.onSubmit(() => {
        this.refs.submitButton.disabled = false;
        this.refs.nextButton.disabled = this.state.activePage === this.state.length;
        this.refs.previousButton.disabled = this.state.activePage === 1;
      });
    }
  }
  render() {
    return (
      <div id="image-tagger-pagination" >
        <button type="button"
          onClick={ this._handleIndicatorClicked.bind(this, -1) }
          disabled={ this.state.activePage === 1 ? 'disabled' : '' }
          ref="previousButton"
          className="image-tagger-previous-page image-tagger-pagination-indicator"
        ></button>
        <form
          className="image-tagger-page-block"
          onSubmit={ this._handlePageInput }
        >
          <input type="text" className="image-tagger-page-num"
            ref="pageInput"
            name="activePage"
            onClick={ this._handlePageInputClick }
            defaultValue={ this.state.activePage }
          />
          <span className="image-tagger-total-pages">{ '/' + this.state.length }</span>
        </form>
        <button type="button"
          ref="nextButton"
          className="image-tagger-next-page image-tagger-pagination-indicator"
          disabled={ this.state.activePage === this.state.length ? 'disabled' : '' }
          onClick={ this._handleIndicatorClicked.bind(this, 1) }
        ></button>
        <div className="image-tagger-form-group">
          <form onSubmit={ this._handleSkip.bind(this) } >
            <input
              className="image-tagger-submit"
              ref="submitButton" type="submit"
              value="跳过"
            />
          </form>
          <form onSubmit={ this._handleSubmit.bind(this) } >
            <input
              className="image-tagger-submit"
              ref="submitButton" type="submit"
              value="提交"
            />
          </form>
        </div>
      </div>
    );
  }
}

export { Pagination as default };
