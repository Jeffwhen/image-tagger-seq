var autoprefixer = require('autoprefixer');
var webpack = require('webpack');

module.exports = {
  entry: {
    app: [
      'babel-polyfill',
      __dirname + '/main.js'
    ],
    vendor: [
      'react-bootstrap',
      'fabric',
      'jquery',
      'react',
      'react-dom'
    ]
  },
  output: {
    path: __dirname + '/dist',
    filename: 'image-tagger-bundle.js'
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015', 'react']
        },
        exclude: /(node_modules|bower_components)/
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader!postcss-loader'
      }
    ],
    postcss: [
      autoprefixer({ browsers: ['> 1%', 'IE 7'] })
    ]
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin(
      "vendor",
      "image-tagger-vendor.js"
    )
  ]
};
