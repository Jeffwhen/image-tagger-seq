import $ from 'jquery';
import React from 'react';
import * as Bootstrap from 'react-bootstrap';

class Edit extends React.Component {
  constructor(props) {
    super(props);
    // `props.onCommand`
    // `props.mode`
    this.state = {};
    if (typeof props.mode !== 'number') {
      var mode = Number(props.mode);
      this.state._mode = Number.isNaN(mode) ? 0x1f : mode;
    }
    else
      this.state._mode = props.mode;
    this.commands = [
      {
        id: 'tag-tool',
        selected: {
          value: 'drag-tag'
        }
      },
      {
        id: 'tag-tool',
        selected: {
          value: 'dot-tag'
        }
      },
      {
        id: 'select-tool',
        selected: {
          value: 'select'
        }
      },
      {
        id: 'select-tool',
        selected: {
          value: 'select-all'
        }
      },
      {
        id: 'select-tool',
        selected: {
          value: 'remove'
        }
      }
    ];
    for (var i = 0; i < this.commands.length; i++) {
      if (Math.pow(2, i) & this.state._mode)
        break;
    }
    this.state.selected = this.commands[i].selected.value;
  }
  triggleCommand() {
    this._handleClick(this.state.selected);
  }
  _handleClick(name) {
    var command = null;
    switch (name) {
      case 'drag-tag':
        command = this.commands[0];
        this.state.selected = name;
        break;
      case 'dot-tag':
        command = this.commands[1];
        this.state.selected = name;
        break;
      case 'select':
        command = this.commands[2];
        this.state.selected = name;
        break;
      case 'select-all':
        this.state.selected = 'select';
        command = this.commands[3];
        break;
      case 'remove':
        command = this.commands[4];
        break;
    }
    this.forceUpdate();
    this.props.onCommand(command);
  }
  render() {
    return (
      <Bootstrap.ButtonGroup
        vertical block
        className="image-tagger-control-button-group"
      >
        <p className="image-tagger-control-group-title">
          工具栏</p>
        <Bootstrap.Button title="拖动画框"
          className="image-tagger-control-button"
          style={{
            display: this.state._mode & 0x01 ? 'block' : 'none',
            backgroundColor: this.state.selected === 'drag-tag' ?
                             '#3f4044' : '#2b2c30'
          }}
          onClick={ this._handleClick.bind(this, 'drag-tag') }
        >
          <span className="image-tagger-drag-tag-icon image-tagger-control-icon">
          </span>
          画框
        </Bootstrap.Button>
        <Bootstrap.Button title="四点画框"
          className="image-tagger-control-button"
          style={{
            display: this.state._mode & 0x02 ? 'block' : 'none',
            backgroundColor: this.state.selected === 'dot-tag' ?
                             '#3f4044' : '#2b2c30'
          }}
          onClick={ this._handleClick.bind(this, 'dot-tag') }
        >
          <span className="image-tagger-click-tag-icon image-tagger-control-icon">
          </span>
          点框
        </Bootstrap.Button>
        <Bootstrap.Button title="选择模式"
          className="image-tagger-control-button"
          style={{
            display: this.state._mode & 0x04 ? 'block' : 'none',
            backgroundColor: this.state.selected === 'select' ?
                             '#3f4044' : '#2b2c30'
          }}
          onClick={ this._handleClick.bind(this, 'select') }
        >
          <span className="image-tagger-select-icon image-tagger-control-icon">
          </span>
          选择
        </Bootstrap.Button>
        <Bootstrap.Button title="全部选择"
          className="image-tagger-control-button"
          style={{
            display: this.state._mode & 0x08 ? 'block' : 'none',
            backgroundColor: this.state.selected === 'select-all' ?
                             '#3f4044' : '#2b2c30'
          }}
          onClick={ this._handleClick.bind(this, 'select-all') }
        >
          <span className="image-tagger-select-all-icon image-tagger-control-icon">
          </span>
          全选
        </Bootstrap.Button>
        <Bootstrap.Button title="删除选中"
          className="image-tagger-control-button"
          style={{
            display: this.state._mode & 0x10 ? 'block' : 'none',
            backgroundColor: this.state.selected === 'remove' ?
                             '#3f4044' : '#2b2c30'
          }}
          onClick={ this._handleClick.bind(this, 'remove') }
        >
          <span className="image-tagger-remove-icon image-tagger-control-icon">
          </span>
          删除
        </Bootstrap.Button>
      </Bootstrap.ButtonGroup>
    );
  }
}

class ColorSelector extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.state.selectedIndex = 0;
    this.styleList = [
      {
        backgroundColor: '#DC143C',
        border: ''
      },
      {
        backgroundColor: '#FFFF00'
      },
      {
        backgroundColor: '#3CB371'
      },
      {
        backgroundColor: '#4169E1'
      },
      {
        backgroundColor: '#9400D3'
      },
      {
        backgroundColor: '#000000'
      }
    ];
    this.getColor = this.getColor.bind(this);
  }
  getColor() {
    return this.styleList[this.state.selectedIndex].backgroundColor;
  }
  _handleClick(index, event) {
    this.state.selectedIndex = index;
    var cellButtons = Array.prototype.slice.call(event.target.parentNode.children);
    cellButtons.forEach((cell, nodeIndex) => {
      if (nodeIndex === index) {
        cell.style.border = "3px solid #CBCBCB";
      } else {
        cell.style.border = "3px solid white";
      }
    });
    this.props.onCommand({
      id: 'color',
      selected: {
        value: this.styleList[index].backgroundColor
      }
    });
  }
  _createItems(num) {
    var items = [];
    this.styleList.forEach((style, itemIndex) => {
      if (this.state.selectedIndex === itemIndex) {
        style.border = '3px solid #CBCBCB';
      } else {
        style.border = '3px solid white';
      }
    });
    for (var i = 0; i < num; i++) {
      items.push(
        <button
        className="image-tagger-color-cell"
        key={ 'color-cell-' + i }
        style={ this.styleList[i] }
        onClick={ this._handleClick.bind(this, i) }
        ></button>
      );
    }
    return items;
  }
  render() {
    return (
      <div id="image-tagger-color-selector">
      { this._createItems(6) }
      </div>
    );
  }
}

class ControlPannel extends React.Component {
  constructor(props) {
    super(props);
    this._handleCommand = this._handleCommand.bind(this);
    this.getColor = this.getColor.bind(this);
  }
  getColor() {
    return this.refs.color.getColor();
  }
  getWorkMode() {
    return this.refs.edit.state.selected;
  }
  triggleCommand() {
    this.refs.edit.triggleCommand();
  }
  // Handle selectors' command, and pass it upward.
  _handleCommand(options) {
    this.props.onCommand(options);
  }
  render() {
    return (
      <div id="image-tagger-control-pannel">
        <Edit
          mode={ this.props.mode }
          onCommand={ this._handleCommand } ref="edit"
        />
        <ColorSelector onCommand={ this._handleCommand } ref="color" />
      </div>
    );
  }
}

export { ControlPannel as default };
