import React from 'react';
import ReactDOM from 'react-dom';
import * as Bootstrap from 'react-bootstrap';

function genFullElem(image) {
  let rects = genRects(image);
  return (
    <div className="image-tagger-thumb-container">
      <img
        className="image-tagger-thumb-image"
        src={ image.url }
      />
      <div
        className="image-tagger-rects-block"
      >
        { rects }
      </div>
    </div>
  );
}

let optionNameMap = {};
class ImageDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      image: {}
    };
  }
  _handleConfirm() {
    this.setState({
      showModal: false
    });
  }
  show(image) {
    this.setState({
      showModal: true,
      image: image
    });
  }
  render() {
    let confirmButton = (
      <Bootstrap.Button onClick={ this._handleConfirm.bind(this) }>
        确定
      </Bootstrap.Button>
    );
    let blacklist = [
      'items', 'uid', 'sections',
      'batches', 'tasks', 'objs', 'upstreamInfo'
    ];
    let image = this.state.image;
    let detailRows = [];
    if (image.items) {
      let content = image.items.map(item => {
        return item.srcId;
      }).join('\n');
      detailRows.push(
        <tr key={ detailRows.length }>
          <td>{ optionNameMap['srcId'] || 'srcId' }</td>
          <td>{ content }</td>
        </tr>
      );
    }
    for (let key in this.state.image) {
      let isBlacked = blacklist.find(i => {
        return i === key;
      });
      if (isBlacked) continue;
      detailRows.push(
        <tr key={ detailRows.length }>
          <td>{ optionNameMap[key] || key }</td>
          <td>{ String(this.state.image[key]) }</td>
        </tr>
      );
    }
    let body = [
      <div key="table">
        <Bootstrap.Table striped bordered condensed hover>
          <thead><tr><th>选项</th><th>值</th></tr></thead>
          <tbody>{ detailRows }</tbody>
        </Bootstrap.Table>
      </div>,
      <div className="full-picture" key="picture">
        { genFullElem(image) }
      </div>
    ];
    return (
      <Bootstrap.Modal
        className="image-detail-modal"
        onHide={ this._handleConfirm.bind(this) }
        show={ this.state.showModal }
      >
        <Bootstrap.Modal.Header><Bootstrap.Modal.Title>
          图片详情
        </Bootstrap.Modal.Title></Bootstrap.Modal.Header>
        <Bootstrap.Modal.Body>
          { body }
        </Bootstrap.Modal.Body>
        <Bootstrap.Modal.Footer>
          { confirmButton }
        </Bootstrap.Modal.Footer>
      </Bootstrap.Modal>
    );
  }
}

function attrOptionForEach(options, callback) {
  if (!options) return;
  options.forEach(option => {
    callback(option);
    attrOptionForEach(option.children || [], callback);
  });
}

function styleObjectToString(obj) {
  let str = '';
  for (var key in obj) {
    str += key + ': ' + obj[key] + ';';
  }
  return str;
}

function genRects(image, callback) {
  let rects = [];
  image.objs = image.objs || [];
  image.objs.forEach(obj => {
    obj.bndBoxes = obj.bndBoxes || [];
    obj.bndBoxes.forEach(box => {
      box.obj = obj;
      rects.push(box);
    });
  });
  rects.sort((a, b) => {
    let sa = (a.xmax - a.xmin) * (a.ymax - a.ymin);
    let sb = (b.xmax - b.xmin) * (b.ymax - b.ymin);
    return sb - sa;
  });
  rects = rects.map(box => {
    let style = null;
    if (box.xmax && box.ymax) {
      style = {
        top: `${box.ymin * 100}%`,
        left: `${box.xmin * 100}%`,
        width: `${(box.xmax - box.xmin) * 100}%`,
        height: `${(box.ymax - box.ymin) * 100}%`
      };
    } else {
      style = {
        border: 0,
        left: 0,
        top: 0
      };
    }
    let attr = (box.attrs || []).map(attr => {
      let values = [];
      attrOptionForEach(attr.values, v => {
        values.push(v.name);
      });
      return attr.name + '-' + values.join('-');
    }).join(' ');
    let nameText = `${box.obj.name} ${attr}`;
    let handleClick = event => {
      event.stopPropagation();
      if (callback)
        callback(box, box.obj);
    };
    let name = <p>{ nameText }</p>;
    let width = box.xmax - box.xmin;
    let height = box.ymax - box.ymin;
    if (width < 0.08 || height < 0.1) {
      name = null;
    }
    return (
      <div
        key={ box.uid }
        className="image-tagger-tag-rect"
        style={ style }
        onClick={ handleClick }
        title={ nameText }
      >
        { name }
      </div>
    );
    });
  return rects;
}

let _thumbWidth = 160;
let _thumbHeight = 150;
function genImageElem(data, thumbWidth, thumbHeight, params) {
  params = params || {};
  _thumbWidth = thumbWidth ? thumbWidth : _thumbWidth;
  _thumbHeight = thumbHeight ? thumbHeight : _thumbHeight;
  let crop = {
    xmin: 1,
    xmax: 0,
    ymin: 1,
    ymax: 0
  };
  let hasCrop = false;
  data.objs = data.objs || [];
  data.objs.forEach(obj => {
    obj.bndBoxes = obj.bndBoxes || [];
    obj.bndBoxes.forEach(box => {
      hasCrop = true;
      crop.xmin = box.xmin < crop.xmin ? box.xmin : crop.xmin;
      crop.xmax = box.xmax > crop.xmax ? box.xmax : crop.xmax;
      crop.ymin = box.ymin < crop.ymin ? box.ymin : crop.ymin;
      crop.ymax = box.ymax > crop.ymax ? box.ymax : crop.ymax;
    });
  });
  if (hasCrop && (crop.xmin > crop.xmax || crop.ymin > crop.ymax)) {
    console.error(data);
    throw new Error('invalid box');
    crop = {
      xmin: 1,
      xmax: 0,
      ymin: 1,
      ymax: 0
    };
  }
  crop = {
    left: crop.xmin - 0.1,
    top: crop.ymin - 0.1,
    width: crop.xmax - crop.xmin + 0.1,
    height: crop.ymax - crop.ymin + 0.1
  };
  if (!hasCrop) {
    crop = {
      left: 0,
      top: 0,
      width: 1,
      height: 1
    };
  }
  for (var key in crop) {
    if (crop[key] < 0)
      crop[key] = 0;
  }
  if (crop.width <= 0.1)
    crop.width = 1;
  if (crop.height <= 0.1)
    crop.height = 1;
  let imgStyle = null;
  let calcStyle = (imgWidth, imgHeight) => {
    let pixelCrop = {
      left: crop.left * imgWidth,
      width: crop.width * imgWidth,
      top: crop.top * imgHeight,
      height: crop.height * imgHeight
    };
    let cropRatio = pixelCrop.width / pixelCrop.height;
    let thumbRatio = _thumbWidth / _thumbHeight;
    let scale = null;
    if (cropRatio >= thumbRatio) {
      // Full width.
      scale = _thumbWidth / pixelCrop.width;
    } else {
      // Full height.
      scale = _thumbHeight / pixelCrop.height;
    }
    pixelCrop.left *= scale;
    pixelCrop.top *= scale;
    pixelCrop.width *= scale;
    pixelCrop.height *= scale;
    imgWidth *= scale;
    imgHeight *= scale;
    let imgStyle = {};
    if (cropRatio >= thumbRatio) {
      // Full width.
      let top = 0;
      if (imgHeight > _thumbHeight) {
        top = (_thumbHeight - pixelCrop.height) / 2 - pixelCrop.top;
        top = top / _thumbHeight;
      }
      imgStyle = {
        top: `${top * 100}%`,
        left: `${- pixelCrop.left / _thumbWidth * 100}%`,
        width: `${imgWidth / _thumbWidth * 100}%`,
        height: `${imgHeight / _thumbHeight * 100}%`,
        position: 'relative',
        visibility: 'visible'
      };
    } else {
      // Full height.
      let left = 0;
      if (imgWidth > _thumbWidth) {
        left = (_thumbWidth - pixelCrop.width) / 2 - pixelCrop.left;
        left = left / _thumbWidth;
      }
      imgStyle = {
        top: `${- pixelCrop.top / _thumbHeight * 100}%`,
        left: `${left * 100}%`,
        width: `${imgWidth / _thumbWidth * 100}%`,
        height: `${imgHeight / _thumbHeight * 100}%`,
        position: 'relative',
        visibility: 'visible'
      };
    }
    return imgStyle;
  };
  let callback = null;
  if (!data.width || !data.height) {
    imgStyle = {
      position: 'fixed',
      visibility: 'hidden'
    };
    let mark = false;
    callback = event => {
      if (mark) return;
      let imgHeight = event.target.naturalHeight;
      let imgWidth = event.target.naturalWidth;
      imgStyle = calcStyle(imgWidth, imgHeight);
      event.target.parentNode.style = styleObjectToString(imgStyle);
      mark = true;
    }
  } else {
    imgStyle = calcStyle(data.width, data.height);
    let mark = false;
    callback = event => {
      mark = true;
    };
  }
  let rects = genRects(data, params.onRectClick);
  let image = (
    <div className="image-tagger-thumb-container" style={ imgStyle }>
      <img
        className="image-tagger-thumb-image"
        src={ data.url }
        onLoad={ callback }
      />
      <div
        className="image-tagger-rects-block"
      >
        { rects }
      </div>
    </div>
  );
  return image;
}

class Sequence extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      imgPage: 0
    };
  }
  _incImgPage(step, event) {
    let page = this.state.imgPage + step;
    let midst = 0;
    let seq = this.props.sequence;
    let imgList = (seq && seq.images || []);
    imgList.find((img, index) => {
      if (img['imgid'] == this.props.current) {
        midst = index;
        return true;
      }
    });
    let destIndex = midst;
    if (page > 0) {
      destIndex += page * 4 - 1;
    } else {
      destIndex += page * 4 + 2;
    }
    if (!imgList[destIndex]) {
      return;
    }
    this.setState({
      imgPage: page
    });
  }
  _seqIndex(index, event) {
    if (this.props.seqIndex) {
      this.props.seqIndex(index);
    }
  }
  _handleSeqInput(event) {
    event.preventDefault();
    let v = event.target.page.value;
    this._seqIndex(v);
  }
  _handleImageClick(img, event) {
    this.refs.imageDetail.show(img);
  }
  componentWillReceiveProps(nextProps) {
    this.state.imgPage = 0;
  }
  render() {
    var seq = this.props.sequence;

    if (seq && seq.maxseqid > seq.seqid) {
      var nextBtn = (
        <Bootstrap.Button
          className="image-tagger-seq-indicator image-tagger-prev-seq"
          onClick={ this._seqIndex.bind(this, seq.seqid + 1) }
        >
          下一序列
        </Bootstrap.Button>
      );
    } else {
      var nextBtn = (
        <Bootstrap.Button
          className="image-tagger-seq-indicator image-tagger-prev-seq"
          disabled
        >
          下一序列
        </Bootstrap.Button>
      );
    }
    if (seq && seq.seqid > 0) {
      var prevBtn = (
        <Bootstrap.Button
          className="image-tagger-seq-indicator image-tagger-next-seq"
          onClick={ this._seqIndex.bind(this, seq.seqid - 1) }
        >
          上一序列
        </Bootstrap.Button>
      );
    } else {
      var prevBtn = (
        <Bootstrap.Button
          className="image-tagger-seq-indicator image-tagger-next-seq"
          disabled
        >
          上一序列
        </Bootstrap.Button>
      );
    }

    var imgList = (seq && seq.images || []);
    var midst = 0;
    var seqStyle = {};
    if (window.innerHeight < 900) {
      seqStyle.top = '490px';
    }
    imgList.find((img, index) => {
      if (img['imgid'] == this.props.current) {
        midst = index;
        return true;
      }
    });
    if (this.state.imgPage) {
      midst += this.state.imgPage * 4;
    }
    var start = Math.max(midst - 1, 0);
    start = Math.min(start, imgList.length - 5);
    imgList = imgList.slice(start, start + 4);
    var elemList = imgList.map(img => {
      return (
        <div
          className="image-tagger-col" key={ img.imgid }
          onClick={ this._handleImageClick.bind(this, img) }
        >
          { genImageElem(img) }
        </div>
      );
    });
    return (
      <div className="image-tagger-sequence" style={ seqStyle }>
        <a
          className="image-tagger-seq-img-prev image-tagger-seq-img-indicator"
          onClick={ this._incImgPage.bind(this, -1) }
        ></a>
        <div className="row">
          { elemList }
        </div>
        <a
          className="image-tagger-seq-img-next image-tagger-seq-img-indicator"
          onClick={ this._incImgPage.bind(this, 1) }
        ></a>
        <div className="image-tagger-seq-pag">
          <form onSubmit={ this._handleSeqInput.bind(this) }>
            <input type="number" name="page"
              className="image-tagger-input image-tagger-seq-page"
            />
            <Bootstrap.Button type="submit">跳转</Bootstrap.Button>
          </form>
          <Bootstrap.Button
            className="image-tagger-seq-boundary"
            onClick={ this._seqIndex.bind(this, 0) }
          >
            首页
          </Bootstrap.Button>
          { prevBtn } { nextBtn }
          <Bootstrap.Button
            className="image-tagger-seq-boundary"
            onClick={ this._seqIndex.bind(this, seq && seq.maxseqid) }
          >
            尾页
          </Bootstrap.Button>
        </div>
        <ImageDetail ref="imageDetail" />
      </div>
    );
  }
}

Sequence.propTypes = {
  current: React.PropTypes.string,
  sequence: React.PropTypes.object,
  seqIndex: React.PropTypes.func
};

export { Sequence as default };
