import { fabric } from 'fabric';
import * as Utils from './utils.js';
import { CropRect, CropMode } from './crop-mode.js';

var SelectPoint = fabric.util.createClass(fabric.Rect, {
  type: 'SelectPoint',
  initialize: function (options) {
    var defaultOptions = {
      width: 8,
      height: 8,
      originX: 'center',
      originY: 'center',
      fill: 'transparent',
      stroke: 'black',
      selectable: false,
      strokeWidth: 1
    };
    options = Utils.extendDict(options, defaultOptions);
    this.callSuper('initialize', options);
  }
});

class ClickCrop extends CropMode {
  constructor(canvas, options) {
    super(canvas, options);
    this._clickCount = 0;
    this._isMouseDown = false;
    this._temporalSelectMode = false;
    this._points = [];

    this._handleMouseDown = this._handleMouseDown.bind(this);
    this._handleMouseUp = this._handleMouseUp.bind(this);
    this._boundMethodHolder = null;
  }
  set stroke(stroke) {
    super.stroke = stroke;
    this._canvas.getObjects('SelectPoint').forEach(point => {
      point.stroke = stroke;
    });
  }
  clearCanvas() {
    super.clearCanvas();
    this._clickCount = 0;
  }
  _preTagMode() {
    this._canvas.on('mouse:down', this._handleMouseDown);
    this._canvas.on('mouse:up', this._handleMouseUp);
  }
  _afterTagMode() {
    this._canvas.off('mouse:down', this._handleMouseDown);
    this._canvas.off('mouse:up', this._handleMouseUp);
  }
  _crop() {
    if (this._clickCount < 4) return;
    this._clickCount = 0;
    var points = this._canvas.getObjects('SelectPoint');
    var maxTop = 0, minTop = this._canvas.getHeight();
    var maxLeft = 0, minLeft = this._canvas.getWidth();
    points.forEach((point) => {
      var top = point.top, left = point.left;
      maxTop = Utils.max(maxTop, top);
      minTop = Utils.min(minTop, top);
      maxLeft = Utils.max(maxLeft, left);
      minLeft = Utils.min(minLeft, left);
      this._canvas.remove(point);
    });
    var width = maxLeft - minLeft;
    var height = maxTop - minTop;
    this._transientCropIndex = this._addCrop({
      stroke: this._stroke,
      strokeWidth: this._strokeWidth,
      left: minLeft,
      top: minTop,
      width: width,
      height: height
    });
    var crop = this._crops[this._transientCropIndex];
    this._enterTemporalSelectMode(crop);
    this.refresh();
  }
  // Disable select.
  _handleCropDeselected(crop) {
    if (this._workMode !== 'tag-mode')
      return;
    this._preTagMode();
    this._temporalSelectMode = false;
    crop.lockUp();
    this._canvas.selection = false;
    crop.off('deselected', this._boundMethodHolder);
  }
  _handleContainerContextMenu(event) {
    super._handleContainerContextMenu(event);
    var crop = this.getActiveCrops()[0];
    if (crop)
      this._enterTemporalSelectMode(crop);
  }
  _enterTemporalSelectMode(crop) {
    this.setActiveCrop(crop.uid);
    this._canvas.selection = true;
    crop.unlock();
    // Add select event handler to lockUp crop when deselect.
    this._boundMethodHolder = this._handleCropDeselected.bind(this, crop);
    crop.on('deselected', this._boundMethodHolder);
    // Enable select temporally.
    this._afterTagMode();
    this._temporalSelectMode = true;
  }
  _handleMouseDown(options) {
    if (this._temporalSelectMode) return;
    this._isMouseDown = true;
    var pointer = this._canvas.getPointer(options.e);
    this._clickCount++;
    this._canvas.add(new SelectPoint({
      left: pointer.x,
      top: pointer.y,
      stroke: this._stroke
    }));
  }
  _handleMouseUp(options) {
    if (!this._isMouseDown) return;
    this._isMouseDown = false;
    if (this._clickCount >= 4)
      this._crop();
  }
}

export { ClickCrop as default };
