import { fabric } from 'fabric';
import { CropRect, CropMode } from './crop-mode.js';

class ClickNDragCrop extends CropMode {
  constructor(canvas, options) {
    super(canvas, options);
    this._transientCropIndex = null;
    this._isMouseDown = false;
    this._origX = null;
    this._origY = null;
    this._handleMouseDown = this._handleMouseDown.bind(this);
    this._handleMouseUp = this._handleMouseUp.bind(this);
    this._handleMouseMove = this._handleMouseMove.bind(this);
    this._boundMethodHolder = null;
  }
  _preTagMode() {
    this._canvas.on('mouse:down', this._handleMouseDown);
    this._canvas.on('mouse:up', this._handleMouseUp);
    this._canvas.on('mouse:move', this._handleMouseMove);
  }
  _afterTagMode() {
    this._canvas.off('mouse:down', this._handleMouseDown);
    this._canvas.off('mouse:up', this._handleMouseUp);
    this._canvas.off('mouse:move', this._handleMouseMove);
  }
  // Disable select.
  _handleCropDeselected(crop) {
    if (this._workMode !== 'tag-mode')
      return;
    this._preTagMode();
    crop.lockUp();
    this._canvas.selection = false;
    crop.off('deselected', this._boundMethodHolder);
  }
  _handleContainerContextMenu(event) {
    super._handleContainerContextMenu(event);
    var crop = this.getActiveCrops()[0];
    if (crop)
      this._enterTemporalSelectMode(crop);
  }
  _enterTemporalSelectMode(crop) {
    this.setActiveCrop(crop.uid);
    this._canvas.selection = true;
    crop.unlock();
    // Add select event handler to lockUp crop when deselect.
    this._boundMethodHolder = this._handleCropDeselected.bind(this, crop);
    crop.on('deselected', this._boundMethodHolder);
    // Enable select temporally.
    this._afterTagMode();
  }
  _handleMouseDown(options) {
    this._isMouseDown = true;
    // temporal select mode. TODO

    var pointer = this._canvas.getPointer(options.e);
    this._origX = pointer.x < 0 ? 0 : pointer.x;
    this._origY = pointer.y < 0 ? 0 : pointer.y;
    var pointer = this._canvas.getPointer(options.e);
    this._transientCropIndex = this._addCrop({
      stroke: this._stroke,
      strokeWidth: this._strokeWidth,
      width: pointer.x - this._origX,
      height: pointer.y - this._origY,
      left: this._origX,
      top: this._origY,
      fill: 'rgba(0, 0, 0, 0.15)'
    });
    var crop = this._crops[this._transientCropIndex];
    crop.lockUp();
    this.refresh();
  }
  _handleMouseMove(options) {
    var validity = this._isMouseDown &&
                   typeof this._transientCropIndex === 'number';
    if (!validity) {
      return;
    }
    var pointer = this._canvas.getPointer(options.e);
    if (pointer.x < 1) pointer.x = 1 + this._strokeWidth;
    if (pointer.y < 1) pointer.y = 1 + this._strokeWidth;
    if (pointer.x >= this._canvas.getWidth())
      pointer.x = this._canvas.getWidth() - this._strokeWidth - 1;
    if (pointer.y >= this._canvas.getHeight())
      pointer.y = this._canvas.getHeight() - this._strokeWidth - 1;
    var left = this._origX, top = this._origY;
    if (pointer.x < this._origX) {
      left = pointer.x;
    }
    if (pointer.y < this._origY) {
      top = pointer.y;
    }
    var width = Math.abs(pointer.x - this._origX);
    var height = Math.abs(pointer.y - this._origY);
    var crop = this._crops[this._transientCropIndex];
    crop.set({
      left: left,
      top: top,
      width: width,
      height: height
    });
    crop.setCoords();
    this.refresh();
  }
  _handleMouseUp(options) {
    if (!this._isMouseDown) return;
    this._isMouseDown = false;
    if (typeof this._transientCropIndex !== 'number') return;
    var crop = this._crops[this._transientCropIndex];
    if (Math.abs(crop.getWidth()) < 4) {
      this.removeCrop(this._transientCropIndex);
      return;
    }
    this._enterTemporalSelectMode(crop);
    this.refresh();
  }
}

export { ClickNDragCrop as default };
