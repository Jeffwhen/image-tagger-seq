import { fabric } from 'fabric';
import * as Utils from './utils.js';

var CropRect = fabric.util.createClass(fabric.Rect, {
  type: 'CropRect',
  initialize: function (options) {
    var defaultOptions = {
      originX: 'left',
      originY: 'top',
      transparentCorners: false,
      lockRotation: true,
      hasRotatingPoint: false,
      hasBorders: false,
      borderColor: '#505050',
      cornerColor: '#505050',
      fill: 'transparent',
      stroke: 'black',
      strokeWidth: 1,
      selection: false,
      name: 'Crop REct',
      cornerSize: 6
    };
    options = Utils.extendDict(options, defaultOptions);
    this.objectId = options.objectId; // Public objectId
    this.upstream = options.upstream;

    this._taggerId = options.uid;
    this.name = options.name || '';
    if (options.editable === false)
      this._editable = false;
    else
      this._editable = true;
    this.description = options.description || '';
    this.callSuper('initialize', options);
    var t = options.hasTransient;
    this._taggerIsTransient = typeof t === 'boolean' ? t : true;
    this.set('objectType', options.objectType || '');
    this.on('deselected', this._handleDeselected.bind(this));
  },
  get uid() {
    return this._taggerId;
  },
  lockUp() {
    this.lockMovementX = true;
    this.lockMovementY = true;
    this.lockScalingFlip = true;
    this.lockScalingX = true;
    this.lockScalingY = true;
    this.lockSkewingX = true;
    this.lockSkewingY = true;
    this.lockUniScaling = true;
    this.selectable = false;
    this.hoverCursor = 'default';
  },
  unlock() {
    this.selectable = true;
    this.hoverCursor = 'pointer';
    if (!this._editable) {
      console.log('ImageTaggerWarning: crop not editable.');
      return;
    }
    this.lockMovementX = false;
    this.lockMovementY = false;
    this.lockScalingFlip = false;
    this.lockScalingX = false;
    this.lockScalingY = false;
    this.lockSkewingX = false;
    this.lockSkewingY = false;
    this.lockUniScaling = false;
  },
  acomplish: function () {
    this.fill = 'transparent';
    this._taggerIsTransient = false;
    this.setCoords();
  },
  _render: function (ctx) {
    this.callSuper('_render', ctx);
    if (this.scaleX !== 1) {
      this.width = this.width * this.scaleX;
      this.scaleX = 1;
    }
    if (this.scaleY !== 1) {
      this.height = this.height * this.scaleY;
      this.scaleY = 1;
    }

    // Set original scale
    var flipX = this.flipX ? -1 : 1;
    var flipY = this.flipY ? -1 : 1;
    var scaleX = flipX / this.scaleX;
    var scaleY = flipY / this.scaleY;
    ctx.scale(scaleX, scaleY);
    if (this._taggerIsTransient)
      this._renderGrid(ctx);
    this._renderText(ctx);
    ctx.scale(1 / scaleX, 1 / scaleY);
  },
  _renderText: function (ctx) {
    ctx.save();
    ctx.translate(-this.getWidth() / 2, -this.getHeight() / 2);
    ctx.font = '14px sans-serif';
    ctx.fillStyle = this.stroke;
    var text = this.name + ' ' + this.description;
    ctx.fillText(text, 4, 16);
    ctx.restore();
  },
  _renderGrid: function (ctx) {
    var drawLine = function (x, y, tx, ty) {
      ctx.beginPath();
      ctx.moveTo(x, y);
      ctx.lineTo(tx, ty);
      ctx.stroke();
    }
    var width = this.getWidth();
    var height = this.getHeight();
    ctx.save();
    ctx.lineWidth = 2;
    ctx.strokeStyle = 'rgba(255, 255, 255, 0.4)';
    ctx.translate(-this.getWidth() / 2, -this.getHeight() / 2);
    drawLine(width / 3, 0, width / 3, height);
    drawLine(width * 2 / 3, 0, width * 2 / 3, height);
    drawLine(0, height / 3, width, height / 3);
    drawLine(0, 2 * height / 3, width, 2 * height / 3);
    ctx.restore();
  },
  _handleDeselected: function (options) {
    this._taggerIsTransient = false;
    this.set({
      fill: 'transparent'
    });
  }
});

//********************************************************************
// CropMode
// The CropMode base class handles all basic status of crop mode
// instances. But it's more of an abstract class. Any implement should
// handle the rects construction by implement `_preTagMode` method and
// `_afterTagMode`.
//********************************************************************
class CropMode {
  constructor(canvas, options) {
    if (this.constructor === CropMode) {
      var msg = 'Abstract class "CropMode" ' +
                'cannot be instantiated directly.';
      throw new TypeError(msg);
    }
    if (!(canvas instanceof fabric.Canvas)) {
      var msg = 'CropMode constructor must take a Canvas instance.';
      throw new TypeError(msg);
    }
    this._canvas = canvas;
    this._containerElement = this._canvas.getElement().parentNode;
    this._imageElement = document.getElementById(
      'image-tagger-original-image'
    );
    if (!this._imageElement) {
      this._imageElement = document.createElement('img');
      this._imageElement.style.visibility = 'hidden';
      this._imageElement.style.position = 'fixed';
      this._imageElement.style.top = 0;
      this._imageElement.style.left = 0;
      this._imageElement.id = 'image-tagger-original-image';
      this._containerElement.appendChild(this._imageElement);
    }
    this._imageObject = new fabric.Image();
    this._stroke = options && options.stroke || 'black';
    this._strokeWidth = options && options.strokeWidth || 2;
    this._workMode = null;
    this._indirectCrops = [];
    this._name = options && options.name || null;
    this._description = options && options.description || null;
    this._objectId = options && options.objectId || null;

    this._handleCropSelected = null;

    // Canvas initialize.
    this._canvas.selection = false;
    this._canvas.selectionLineWidth = 0;
    
    this._handleContainerContextMenu =
      this._handleContainerContextMenu.bind(this);
    this._canvas.getElement().parentNode.addEventListener(
      'contextmenu',
      this._handleContainerContextMenu
    );
  }
  // Set name for current selected crops.
  set name(name) {
    if (typeof name !== 'string') {
      console.error('ImageTaggerError: crop name should be string.');
      throw new Error('Bad Argument!');
    }
    this._name = name;
    this.getActiveCrops().forEach(crop => {
      crop.name = name;
    });
  }
  get name() {
    return this._name;
  }
  // Set object for current selected crops.
  set objectId(objectId) {
    this._objectId = objectId;
    this.getActiveCrops().forEach(crop => {
      crop.objectId = objectId;
      crop.upstream = false;
    });
  }
  // Set description for current selected crops.
  set description(description) {
    if (typeof description !== 'string') {
      console.error('ImageTaggerError: crop description should be string.');
      throw new Error('Bad Argument!');
    }
    this._description = description;
    this.getActiveCrops().forEach(crop => {
      crop.description = description;
    });
  }
  get stroke() {
    return this._stroke;
  }
  set stroke(stroke) {
    if (typeof stroke !== 'string') {
      console.error('ImageTaggerError: CropMode.stroke should be string.');
      throw new Error('Bad Argument!');
    }
    this._stroke = stroke;
    this.getActiveCrops().forEach(crop => {
      crop.stroke = stroke;
    });
  }
  get strokeWidth() {
    return this._strokeWidth;
  }
  set strokeWidth(strokeWidth) {
    if (typeof strokeWidth !== 'number') {
      console.error('ImageTaggerError: CropMode.strokeWidth should be number.');
      throw new Error('Bad Argument!');
    }
    this._strokeWidth = strokeWidth;
  }
  set onCropSelected(callback) {
    this._handleCropSelected = function (options) {
      var uid = options.target.uid;
      if (typeof uid !== 'number') return;
      callback(this._crops[uid]);
    }.bind(this);
    this._canvas.on('object:selected', this._handleCropSelected);
  }
  clearCanvas() {
    this._canvas.clear();
    this._crops = [];
  }
  getRects() {
    return this._validCrops.map(crop => {
      return {
        uid: crop.uid,
        objectId: crop.objectId,
        stroke: crop.stroke,
        strokeWidth: crop.strokeWidth,
        name: crop.name,
        width: crop.width / this._canvas.getWidth(),
        height: crop.height / this._canvas.getHeight(),
        left: crop.left / this._canvas.getWidth(),
        top: crop.top / this._canvas.getHeight(),
        attrs: crop.attrs
      };
    });
  }
  getCrops() {
    return this._validCrops;
  }
  // Add single crop or multiple crops as an array into canvas.
  // Derived class should always use this method instead of add crop
  // directly, because this method allocates unque id for crop.
  addCrops(rects, optionalFields) {
    optionalFields = optionalFields || [];
    var index = null;
    if (!(rects instanceof Array)) {
      var rect = rects;
      rect.hasTransient = false;
      index = this._addCrop(rect);
      optionalFields.forEach(field => {
        this._crops[index][field] = rect[field];
      });
      this._crops[index].acomplish();
      this._crops[index].lockUp();
    } else {
      rects.forEach(rect => {
        rect.hasTransient = false;
        var i = this._addCrop(rect);
        optionalFields.forEach(field => {
          this._crops[i][field] = rect[field];
        });
        this._crops[i].acomplish();
        this._crops[i].lockUp();
        if (!index) index = i;
      });
    }
    if (this._workMode === 'select-mode') {
      this.selectMode();
    }
    return index;
  }
  // Remove crop by uid.
  removeCrop(uid) {
    this._crops[uid].remove();
    this._crops[uid] = null;
  }
  // Remove selected crops.
  removeCrops() {
    this.getActiveCrops().forEach(crop => {
      this._crops[crop.uid] = null;
      crop.remove();
    });
    this._canvas.discardActiveGroup();
  }
  setActiveCrop(index) {
    this._canvas.setActiveObject(this._crops[index]);
  }
  refresh() {
    this._canvas.renderAll();
  }
  // Enter tag mode.
  // NOTE: You should refresh to enable the changes.
  tagMode() {
    if (this._workMode === 'tag-mode') return;
    this._deactivateAll();
    this._canvas.selectionLineWidth = 0;
    this._canvas.selection = false;
    this._workMode = 'tag-mode';
    this._validCrops.forEach(crop => {
      crop.lockUp();
    });
    if (this._preTagMode)
      this._preTagMode();
  }
  // This method should be called whenever a CropMode
  // instance is abandened.
  cleanup() {
    this._orphanCare();
    if (this._afterTagMode)
      this._afterTagMode();
    this._validCrops.forEach(crop => {
      crop.remove();
    });
    this._canvas.getElement().parentNode.removeEventListener(
      'contextmenu',
      this._handleContainerContextMenu
    );
    if (this._handleCropSelected)
      this._canvas.off('object:selected', this._handleCropSelected);
  }
  getActiveCrops() {
    var activeObj = this._canvas.getActiveObject();
    var activeGrp = this._canvas.getActiveGroup();
    if (activeGrp)
      return activeGrp.getObjects();
    if (activeObj)
      return [ activeObj ];
    return [];
  }
  setActiveCrop(index) {
    this._canvas.setActiveObject(this._crops[index]);
  }
  setActiveCrops(indexList) {
    if (indexList.length === 1) {
      this.setActiveCrop(indexList);
      return;
    }
    this._canvas.discardActiveGroup();
    var crops = indexList.map(index => {
      return this._crops[index];
    });
    var group = new fabric.Group(crops);
    this._canvas.setActiveGroup(group.setCoords());
  }
  get naturalWidth() {
    return this._naturalWidth;
  }
  get naturalHeight() {
    return this._naturalHeight;
  }
  get canvasWidth() {
    return this._canvas.getWidth();
  }
  get canvasHeight() {
    return this._canvas.getHeight();
  }
  _handleContainerContextMenu(event) {
    event.preventDefault();
    var point = new fabric.Point(event.layerX, event.layerY);
    var crop;
    var validCrops = this._validCrops;
    for (var i in validCrops) {
      if (validCrops[i].containsPoint(point)) {
        if (!crop) {
          crop = validCrops[i];
          continue;
        }
        if (validCrops[i].getWidth() < crop.getWidth() &&
            validCrops[i].getHeight() < crop.getHeight())
          crop = validCrops[i];
        break;
      }
    }
    if (crop) {
      this._deactivateAll();
      this.setActiveCrop(crop.uid);
    } else {
      this._deactivateAll();
    }
  }
  // Update background image.
  // NOTE: You should refresh to enable the changes.
  updateImage(imageURL, callback) {
    if (!/https?:\/\//.test(imageURL)) {
      throw new Error('Bad image URL.');
    }
    this._imageElement.src = imageURL;
    var haveExecuted = false;
    this._imageElement.addEventListener('load', () => {
      if (haveExecuted) return;
      haveExecuted = true;
      this._naturalWidth = this._imageElement.naturalWidth;
      this._naturalHeight = this._imageElement.naturalHeight;
      var ratio = this._imageElement.naturalWidth;
      ratio = ratio / this._imageElement.naturalHeight;
      var height, width;
      if (ratio < 16 / 9) {
        height = this._canvas.getHeight();
        width = height * ratio;
      } else {
        width = this._canvas.getHeight() * 16 / 9;
        height = width / ratio;
      }
      this._imageElement.style.display = 'none';
      this._canvas.setHeight(height);
      this._canvas.setWidth(width);
      this._imageObject.setElement(this._imageElement, null, {
        height: height,
        width: width,
        selectable: false
      });
      this._canvas.setBackgroundImage(this._imageObject, callback);
    });
    this._imageElement.addEventListener('error', (event) => {
      event.target.src = imageURL.replace(
        'img.adkalava.com', 'pic.adkalava.com'
      );
    });
  }
  // Enter select mode.
  // NOTE: You should refresh to enable the changes.
  selectMode() {
    this._orphanCare();
    this._validCrops.forEach(crop => {
      crop.unlock();
    });
    if (this._afterTagMode)
      this._afterTagMode();
    this._workMode = 'select-mode';
  }
  _orphanCare() {
    var crops = this.getActiveCrops();
    crops.forEach(crop => {
      // Use deselected event to trigger acomplish.
      // Not perfect but got to.
      crop.trigger('deselected');
    });
  }
  // Add crop into canvas.
  // Rect format:
  // width, height, left, right, stroke (optional),
  // strokeWidth (optional), name (optional).
  // NOTE: You should refresh to enable the changes.
  _addCrop(rect) {
    if (!rect.strokeWidth) rect.strokeWidth = this._strokeWidth;
    if (!rect.stroke) rect.stroke = this._stroke;
    rect = this._normalize(rect);
    var index;
    if (typeof rect.uid !== 'number' || Number.isNaN(rect.uid))
      index = this._crops.length;
    else
      index = rect.uid;
    rect.uid = index;
    if (!rect.name)
      rect.name = this._name || '';
    if (typeof rect.objectId !== 'number' || Number.isNaN(rect.objectId))
      rect.objectId = this._objectId;
    var crop = new CropRect(rect);
    this._crops[index] = crop;
    this._canvas.add(crop);
    return index;
  }
  // TODO deactivateAllWithDispatch
  _deactivateAll() {
    this._orphanCare();
    var tempRect = new fabric.Rect();
    this._canvas.add(tempRect);
    this._canvas.setActiveObject(tempRect);
    tempRect.remove();

  }
  get _crops() {
    return this._indirectCrops;
  }
  set _crops(crops) {
    this._indirectCrops = crops;
  }
  get _validCrops() {
    var crops = this._indirectCrops.filter(crop => {
      return crop;
    });
    return crops;
  }
  // Convert scaled width, height, left, top into pixels.
  _normalize(options) {
    if (Math.abs(options.width) > 1) return options;
    options.width *= this._canvas.getWidth();
    options.left *= this._canvas.getWidth();
    options.height *= this._canvas.getHeight();
    options.top *= this._canvas.getHeight();
    return options;
  }
}

export { CropMode, CropRect };
