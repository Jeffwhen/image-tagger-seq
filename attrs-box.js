import React from 'react';
import ReactDOM from 'react-dom';
import * as Bootstrap from 'react-bootstrap';

class AttrsBox extends React.Component {
  constructor(props) {
    // `props.onAttrChange`
    // `props.onSelectedObjectChange`
    super(props);
    this.state = {
      objectList: [],
      selectedAttrs: [],
      attrBoardColumns: this.props.attrBoardColumns || 4
    };
    this.state.selectedObject = this.state.objectList[
      this.props.defaultValue || 0];
  }
  //********************************************************************
  // `objects` list of:
  // {
  //    name: 'myName',
  //    uid: 2333,
  //    attrs: [ {
  //      uid: 0,
  //      name: 'attrName',
  //      options: [
  //        { uid: 0, name: '' }
  //      ]
  //    }]
  // }
  //********************************************************************
  update(objects) {
    if (!(objects instanceof Array)) {
      console.error('ImageTaggerError: AttrsBox.update expect a Array.');
      throw new Error('Invalid Argument!');
      return;
    }
    this.state.objectList = objects;
    this.state.selectedObject = this.state.objectList[
      this.props.defaultValue || 0];
    if (this.props.onSelectedObjectChange) {
      this.props.onSelectedObjectChange(this.state.selectedObject);
    }
    this.state.selectedAttrs = [];
    this.forceUpdate();
  }
  getSelectedAttrs() {
    return this.state.selectedAttrs;
  }
  setSelectedAttrs(attrs) {
    this.state.selectedAttrs = attrs;
    this.forceUpdate();
  }
  setSelectedObject(objectId) {
    this.state.selectedObject = this.state.objectList.filter(object => {
      if (object.uid === objectId) return true;
      return false;
    })[0] || [];
    this.forceUpdate();
  }
  getSelectedObject() {
    return this.state.selectedObject;
  }
  _handleAttrButtonClick(attrIndex, optionIndex) {
    var option = this.state.selectedObject.attrs[attrIndex]
                     .options[optionIndex];
    var attr = this.state.selectedObject.attrs[attrIndex];
    var existence = this.state.selectedAttrs.filter(attrItem => {
      return attrItem.uid === attr.uid;
    })[0];
    var index = -1;
    if (existence) {
      attr = existence;
      for (var i = attr.values.length - 1; i > -1; i--) {
        if (attr.values[i].uid === option.uid) {
          attr.values.splice(i, 1);
          index = i;
          break;
        }
      }
      if (index < 0) {
        if (attr.unique)
          attr.values = [option];
        else
          attr.values.push(option);
      }
    } else {
      attr = {
        uid: attr.uid,
        type: attr.type,
        unique: attr.unique || false,
        required: attr.required || false,
        display: attr.display || false,
        name: attr.name,
        values: [ option ]
      };
      this.state.selectedAttrs.push(attr);
    }
    if (index >= 0) {
      this.state.selectedAttrs = this.state.selectedAttrs.filter(attr => {
        return attr.values.length;
      });
    }
    if (this.props.onAttrChange)
      this.props.onAttrChange(this.state.selectedAttrs);
    this.forceUpdate();
  }
  _handleSelectedAttrClick(attrIndex, valueIndex) {
    this.state.selectedAttrs[attrIndex].values.splice(valueIndex, 1);
    if (!this.state.selectedAttrs[attrIndex].values.length)
      this.state.selectedAttrs.splice(attrIndex, 1);
    if (this.props.onAttrChange)
      this.props.onAttrChange(this.state.selectedAttrs);
    this.forceUpdate();
  }
  _handleObjectSelect(index) {
    if (this.state.selectedObject === this.state.objectList[index])
      return;
    this.state.selectedObject = this.state.objectList[index];
    if (this.props.onSelectedObjectChange) {
      this.props.onSelectedObjectChange(this.state.selectedObject);
    }
    this.state.selectedAttrs.splice(0);
    this.forceUpdate();
  }
  _createObjectItems() {
    var objectItems = this.state.objectList.map((object, index) => {
      let className = '';
      let selectedObj = this.state.selectedObject;
      let selectedObjId = selectedObj && selectedObj.uid;
      if (selectedObjId === object.uid ||
          selectedObjId === undefined && index === 0
      ) {
        className = 'active-obj';
      }
      return (
        <Bootstrap.Button
        key={ 'attrBoxObject-' + index }
        className={ className }
          onClick={ this._handleObjectSelect.bind(this, index) }
        >{ object.name }</Bootstrap.Button>
      );
    });
    var newItems = [];
    objectItems.forEach((elem, index) => {
      if (index && !(index % 3)) {
        newItems.push(<br key={ `br-${index}` } />);
      }
      newItems.push(elem);
    });
    return newItems;
  }
  _createSelectedAttrItems() {
    var selectedAttrItems = [];
    this.state.selectedAttrs.forEach((attr, attrIndex) => {
      var buttons = [];
      if (attr.values instanceof Array) {
        buttons = attr.values.map((option, valueIndex) => {
          return <Bootstrap.Button
          key={ 'attrBoxSelectedAttr-' + attrIndex + valueIndex }
          onClick={
            this._handleSelectedAttrClick.bind(this, attrIndex, valueIndex)
          }
          >{ option.name }</Bootstrap.Button>
        });
      }
      selectedAttrItems = selectedAttrItems.concat(buttons);
    });
    return selectedAttrItems;
  }
  _createAttrItems() {
    var trs = [], trCount = 0, tdCount = 0;
    var attrs = this.state.selectedObject &&
                this.state.selectedObject.attrs || [];
    attrs.forEach((attr, attrIndex) => {
      var isSelected = { values: [] };
      this.state.selectedAttrs.forEach(selectedAttr => {
        if (selectedAttr.uid === attr.uid)
          isSelected = selectedAttr;
      });
      attr.options.forEach((option, optionIndex) => {
        var isActive = !!isSelected.values.filter(selectedOption => {
          if (selectedOption.uid === option.uid) return true;
          return false;
        })[0];
        var trInc = Math.floor(tdCount++ / this.state.attrBoardColumns);
        if (trInc) {
          trCount += trInc;
          tdCount = tdCount % this.state.attrBoardColumns;
        }
        if (!trs[trCount]) trs[trCount] = [];
        trs[trCount][tdCount] = (
          <Bootstrap.Button
          onClick={
            this._handleAttrButtonClick.bind(this, attrIndex, optionIndex)
          }
          bsStyle="info"
          active={ isActive }
          >
          { option.name }
          </Bootstrap.Button>
        );
      });
      trCount++;
      tdCount = 0;
    });
    trs = trs.map((tds, trIndex) => {
      return (
        <tr key={ 'attr-board-tr-' + trIndex }>
        {
          tds.map((td, tdIndex) => {
            return <td key={ 'attr-board-td-' + trIndex + tdIndex }>{ td }</td>;
          })
        }
        </tr>
      );
    });
    return (
      <table className="image-tagger-attr-board">
      <tbody>
      { trs }
      </tbody>
      </table>
    );
  }
  render() {
    return (
      <div id="image-tagger-attrs-box">
        <p className="image-tagger-div-title">名称</p>
        <Bootstrap.ButtonGroup>
          { this._createObjectItems() }
        </Bootstrap.ButtonGroup>
        <Bootstrap.ButtonGroup
          className="image-tagger-vertical-button-group" vertical
        >
        </Bootstrap.ButtonGroup>
        <p className="image-tagger-div-title">属性列表</p>
        { this._createAttrItems() }
      </div>
    );
  }
}

export { AttrsBox as default };
