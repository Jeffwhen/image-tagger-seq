const schema = require('js-schema');

var BoundBox = schema({
  xmin: Number,
  ymin: Number,
  xmax: Number,
  ymax: Number,
  uid: Number
});

var Attribute = schema({
  uid: Number,
  name: String,
  options: Array
});

var TagObject = schema({
  uid: Number,
  name: String,
  attrs: Array.of(Attribute)
});

var ImageDataValidate = schema({
  errcode: Number,
  unitid: Number,
  imgid: String,
  maxseqid: Number,
  url: String,
  objs: Array.of(TagObject)
});

export { ImageDataValidate as default };
