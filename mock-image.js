var image = {
  errcode: 0,
  unitid: 3267940,
  imgid: "10034609582419340852",
  maxseqid: 50,
  seqid: 3,
  url: "http:\/\/pic.adkalava.com\/img005\/286\/9e905e6a989db23e.jpg",
  src: "http:\/\/pic.adkalava.com\/img005\/286\/9e905e6a989db23e.jpg",
  description: "图片参考信息示例",
  sequence: {
    seqid: 3,
    maxseqid: 3,
    images: [
      {
        "width": 750,
        "height": 500,
        "imgid": "10033388574809970948",
        "url": "http://pic.adkalava.com/img005/687/8e440a7111973427.jpg",
        "objs": [
          {
            "name": "连衣裙",
            "bndBoxes": [
              {
                "ymax": 0.87931727757351,
                "attrs": [{
                  "name": "廓形_新",
                  "values": [
                    {
                      "name": "A型",
                      "uid": 7647,
                      "children": []
                    }
                  ],
                  "uid": 2733
                }],
                "xmin": 0.3707,
                "ymin": 0.316,
                "uid": 3772062,
                "xmax": 0.6134
              }
            ],
            "uid": 2
          }
        ]
      },
      {
        "width": 750,
        "height": 763,
        "url": "http://pic.adkalava.com/img005/357/111b84a0547496dd.jpg",
        "imgid": "10033445701977477290",
        "objs": [
          {
            "name": "连衣裙",
            "bndBoxes": [
              {
                "ymax": 0.93510157202647,
                "attrs": [
                  {
                    "name": "图案",
                    "values": [
                      {
                        "name": "条纹",
                        "uid": 18,
                        "children": [
                          {
                            "name": "轨迹",
                            "uid": 19,
                            "children": [
                              {
                                "name": "横条纹",
                                "uid": 20,
                                "children": []
                              }
                            ]
                          }
                        ]
                      }
                    ],
                    "uid": 3
                  },
                  {
                    "name": "颜色分类",
                    "values": [
                      {
                        "name": "蓝色",
                        "uid": 74,
                        "children": []
                      },
                      {
                        "name": "灰色",
                        "uid": 76,
                        "children": []
                      }
                    ],
                    "uid": 6
                  }
                ],
                "xmin": 0.30152163054274,
                "ymin": 0.2839172409373,
                "uid": 3772120,
                "xmax": 0.77845770467049
              },
              {
                "ymax": 0.5898,
                "attrs": [
                  {
                    "name": "图案",
                    "values": [
                      {
                        "name": "条纹",
                        "uid": 18,
                        "children": [
                          {
                            "name": "轨迹",
                            "uid": 19,
                            "children": [
                              {
                                "name": "横条纹",
                                "uid": 20,
                                "children": []
                              }
                            ]
                          }
                        ]
                      }
                    ],
                    "uid": 3
                  }
                ],
                "xmin": 0.2707,
                "ymin": 0.2779,
                "uid": 3772122,
                "xmax": 0.752
              }
            ],
            "uid": 2
          }
        ]
      },
      {
        "width": 750,
        "height": 500,
        "url": "http://pic.adkalava.com/img005/210/af5ec8fa73bdfb92.jpg",
        "imgid": "10033665996586426309",
        "objs": [
          {
            "name": "连衣裙",
            "bndBoxes": [
              {
                "ymax": 0.8394085765338,
                "attrs": [
                  {
                    "name": "廓形_新",
                    "values": [
                      {
                        "name": "A型",
                        "uid": 7647,
                        "children": []
                      }
                    ],
                    "uid": 2733
                  },
                  {
                    "name": "领型",
                    "values": [
                      {
                        "name": "V领",
                        "uid": 7588,
                        "children": []
                      }
                    ],
                    "uid": 2728
                  },
                  {
                    "name": "袖长",
                    "values": [
                      {
                        "name": "短袖",
                        "uid": 7533,
                        "children": []
                      }
                    ],
                    "uid": 2725
                  },
                  {
                    "name": "面料",
                    "values": [
                      {
                        "name": "其他",
                        "uid": 5911,
                        "children": []
                      }
                    ],
                    "uid": 2114
                  },
                  {
                    "name": "图案",
                    "values": [
                      {
                        "name": "格子",
                        "uid": 29,
                        "children": [
                          {
                            "name": "大小",
                            "uid": 30,
                            "children": [
                              {
                                "name": "小格子",
                                "uid": 32,
                                "children": []
                              }
                            ]
                          }
                        ]
                      }
                    ],
                    "uid": 3
                  },
                  {
                    "name": "裙长",
                    "values": [
                      {
                        "name": "短裙",
                        "uid": 65,
                        "children": []
                      }
                    ],
                    "uid": 5
                  },
                  {
                    "name": "颜色分类",
                    "values": [
                      {
                        "name": "白色",
                        "uid": 71,
                        "children": []
                      },
                      {
                        "name": "灰色",
                        "uid": 76,
                        "children": []
                      }
                    ],
                    "uid": 6
                  },
                  {
                    "name": "裙边",
                    "values": [
                      {
                        "name": "齐边",
                        "uid": 5917,
                        "children": []
                      }
                    ],
                    "uid": 2115
                  },
                  {
                    "name": "廓形",
                    "values": [
                      {
                        "name": "A型",
                        "uid": 7424,
                        "children": []
                      }
                    ],
                    "uid": 2702
                  },
                  {
                    "name": "裙摆",
                    "values": [
                      {
                        "name": "竖褶裙",
                        "uid": 7487,
                        "children": []
                      }
                    ],
                    "uid": 2722
                  }
                ],
                "xmin": 0.3507,
                "ymin": 0.284,
                "uid": 3772326,
                "xmax": 0.6267
              }
            ],
            "uid": 2
          }
        ]
      },
      {
        "width": 750,
        "height": 773,
        "url": "http://pic.adkalava.com/img005/857/b28d2daf089d9509.jpg",
        "imgid": "10033682509555763589",
        "objs": [
          {
            "name": "连衣裙",
            "bndBoxes": [
              {
                "ymax": 0.83626220881319,
                "attrs": [
                  {
                    "name": "图案",
                    "values": [
                      {
                        "name": "条纹",
                        "uid": 18,
                        "children": [
                          {
                            "name": "轨迹",
                            "uid": 19,
                            "children": [
                              {
                                "name": "横条纹",
                                "uid": 20,
                                "children": []
                              }
                            ]
                          }
                        ]
                      }
                    ],
                    "uid": 3
                  },
                  {
                    "name": "颜色分类",
                    "values": [
                      {
                        "name": "灰色",
                        "uid": 76,
                        "children": []
                      },
                      {
                        "name": "蓝色",
                        "uid": 74,
                        "children": []
                      }
                    ],
                    "uid": 6
                  }
                ],
                "xmin": 0.54964916415447,
                "ymin": 0.30717356169337,
                "uid": 3772448,
                "xmax": 0.87744318180556
              },
              {
                "ymax": 0.5523,
                "attrs": [
                  {
                    "name": "图案",
                    "values": [
                      {
                        "name": "条纹",
                        "uid": 18,
                        "children": [
                          {
                            "name": "轨迹",
                            "uid": 19,
                            "children": [
                              {
                                "name": "横条纹",
                                "uid": 20,
                                "children": []
                              }
                            ]
                          }
                        ]
                      }
                    ],
                    "uid": 3
                  }
                ],
                "xmin": 0.5627,
                "ymin": 0.2768,
                "uid": 3772449,
                "xmax": 0.864
              }
            ],
            "uid": 2
          }
        ]
      },
      {
        "width": 750,
        "height": 812,
        "url": "http://pic.adkalava.com/img005/848/bbe656bf64c46980.jpg",
        "imgid": "1003374026227448622",
        "objs": [
          {
            "name": "连衣裙",
            "bndBoxes": [
              {
                "ymax": 0.89723670683825,
                "attrs": [
                  {
                    "name": "领型",
                    "values": [
                      {
                        "name": "圆领",
                        "uid": 7587,
                        "children": []
                      }
                    ],
                    "uid": 2728
                  },
                  {
                    "name": "袖长",
                    "values": [
                      {
                        "name": "长袖",
                        "uid": 7532,
                        "children": []
                      }
                    ],
                    "uid": 2725
                  },
                  {
                    "name": "图案",
                    "values": [
                      {
                        "name": "格子",
                        "uid": 29,
                        "children": [
                          {
                            "name": "大小",
                            "uid": 30,
                            "children": [
                              {
                                "name": "错误",
                                "uid": 5890,
                                "children": []
                              }
                            ]
                          }
                        ]
                      }
                    ],
                    "uid": 3
                  },
                  {
                    "name": "裙长",
                    "values": [
                      {
                        "name": "中裙",
                        "uid": 66,
                        "children": []
                      }
                    ],
                    "uid": 5
                  },
                  {
                    "name": "颜色分类",
                    "values": [
                      {
                        "name": "黑色",
                        "uid": 70,
                        "children": []
                      }
                    ],
                    "uid": 6
                  },
                  {
                    "name": "面料",
                    "values": [
                      {
                        "name": "针织",
                        "uid": 5902,
                        "children": []
                      }
                    ],
                    "uid": 2114
                  },
                  {
                    "name": "裙边",
                    "values": [
                      {
                        "name": "齐边",
                        "uid": 5917,
                        "children": []
                      }
                    ],
                    "uid": 2115
                  },
                  {
                    "name": "廓形",
                    "values": [
                      {
                        "name": "H型",
                        "uid": 7425,
                        "children": []
                      }
                    ],
                    "uid": 2702
                  },
                  {
                    "name": "裙摆",
                    "values": [
                      {
                        "name": "常规",
                        "uid": 7483,
                        "children": []
                      }
                    ],
                    "uid": 2722
                  }
                ],
                "xmin": 0.312,
                "ymin": 0.298,
                "uid": 3772457,
                "xmax": 0.7053
              }
            ],
            "uid": 2
          }
        ]
      },
      {
        "width": 750,
        "height": 951,
        "url": "http://pic.adkalava.com/img005/588/12cc73f022946184.jpg",
        "imgid": "10033941427912591755",
        "objs": [
          {
            "name": "连衣裙",
            "bndBoxes": [
              {
                "ymax": 0.97549817421439,
                "attrs": [
                  {
                    "name": "廓形_新",
                    "values": [
                      {
                        "name": "A型",
                        "uid": 7647,
                        "children": []
                      }
                    ],
                    "uid": 2733
                  },
                  {
                    "name": "领型",
                    "values": [
                      {
                        "name": "V领",
                        "uid": 7588,
                        "children": []
                      }
                    ],
                    "uid": 2728
                  },
                  {
                    "name": "袖长",
                    "values": [
                      {
                        "name": "短袖",
                        "uid": 7533,
                        "children": []
                      }
                    ],
                    "uid": 2725
                  },
                  {
                    "name": "面料",
                    "values": [
                      {
                        "name": "其他",
                        "uid": 5911,
                        "children": []
                      }
                    ],
                    "uid": 2114
                  },
                  {
                    "name": "图案",
                    "values": [
                      {
                        "name": "格子",
                        "uid": 29,
                        "children": [
                          {
                            "name": "大小",
                            "uid": 30,
                            "children": [
                              {
                                "name": "小格子",
                                "uid": 32,
                                "children": []
                              }
                            ]
                          }
                        ]
                      }
                    ],
                    "uid": 3
                  },
                  {
                    "name": "裙长",
                    "values": [
                      {
                        "name": "短裙",
                        "uid": 65,
                        "children": []
                      }
                    ],
                    "uid": 5
                  },
                  {
                    "name": "颜色分类",
                    "values": [
                      {
                        "name": "灰色",
                        "uid": 76,
                        "children": []
                      },
                      {
                        "name": "白色",
                        "uid": 71,
                        "children": []
                      }
                    ],
                    "uid": 6
                  },
                  {
                    "name": "裙边",
                    "values": [
                      {
                        "name": "齐边",
                        "uid": 5917,
                        "children": []
                      }
                    ],
                    "uid": 2115
                  },
                  {
                    "name": "廓形",
                    "values": [
                      {
                        "name": "A型",
                        "uid": 7424,
                        "children": []
                      }
                    ],
                    "uid": 2702
                  },
                  {
                    "name": "裙摆",
                    "values": [
                      {
                        "name": "竖褶裙",
                        "uid": 7487,
                        "children": []
                      }
                    ],
                    "uid": 2722
                  }
                ],
                "xmin": 0.2187,
                "ymin": 0.2913,
                "uid": 3772465,
                "xmax": 0.86
              }
            ],
            "uid": 2
          }
        ]
      },
      {
        "width": 750,
        "height": 1116,
        "url": "http://pic.adkalava.com/img005/251/ca90f81744c59cf3.jpg",
        "imgid": "10033962417949659588",
        "objs": [
          {
            "name": "连衣裙",
            "bndBoxes": [
              {
                "ymax": 0.62262200830252,
                "attrs": [
                  {
                    "name": "图案",
                    "values": [
                      {
                        "name": "条纹",
                        "uid": 18,
                        "children": [
                          {
                            "name": "轨迹",
                            "uid": 19,
                            "children": [
                              {
                                "name": "错误",
                                "uid": 5893,
                                "children": []
                              }
                            ]
                          }
                        ]
                      }
                    ],
                    "uid": 3
                  },
                  {
                    "name": "颜色分类",
                    "values": [
                      {
                        "name": "蓝色",
                        "uid": 74,
                        "children": []
                      }
                    ],
                    "uid": 6
                  }
                ],
                "xmin": 0.564,
                "ymin": 0.2464,
                "uid": 3772510,
                "xmax": 0.928
              },
              {
                "ymax": 0.4704,
                "attrs": [
                  {
                    "name": "图案",
                    "values": [
                      {
                        "name": "条纹",
                        "uid": 18,
                        "children": [
                          {
                            "name": "轨迹",
                            "uid": 19,
                            "children": [
                              {
                                "name": "错误",
                                "uid": 5893,
                                "children": []
                              }
                            ]
                          }
                        ]
                      }
                    ],
                    "uid": 3
                  }
                ],
                "xmin": 0,
                "ymin": 0.2894,
                "uid": 3772511,
                "xmax": 0.3627
              }
            ],
            "uid": 2
          }
        ]
      },
      {
        "width": 750,
        "height": 500,
        "url": "http://pic.adkalava.com/img005/673/cb54c178af158991.jpg",
        "imgid": "10034001596212237323",
        "objs": [
          {
            "name": "连衣裙",
            "bndBoxes": [
              {
                "ymax": 0.88592360920483,
                "attrs": [
                  {
                    "name": "廓形_新",
                    "values": [
                      {
                        "name": "A型",
                        "uid": 7647,
                        "children": []
                      }
                    ],
                    "uid": 2733
                  },
                  {
                    "name": "领型",
                    "values": [
                      {
                        "name": "V领",
                        "uid": 7588,
                        "children": []
                      }
                    ],
                    "uid": 2728
                  },
                  {
                    "name": "袖长",
                    "values": [
                      {
                        "name": "短袖",
                        "uid": 7533,
                        "children": []
                      }
                    ],
                    "uid": 2725
                  },
                  {
                    "name": "面料",
                    "values": [
                      {
                        "name": "其他",
                        "uid": 5911,
                        "children": []
                      }
                    ],
                    "uid": 2114
                  },
                  {
                    "name": "图案",
                    "values": [
                      {
                        "name": "格子",
                        "uid": 29,
                        "children": [
                          {
                            "name": "大小",
                            "uid": 30,
                            "children": [
                              {
                                "name": "大小不一",
                                "uid": 33,
                                "children": []
                              }
                            ]
                          }
                        ]
                      }
                    ],
                    "uid": 3
                  },
                  {
                    "name": "裙长",
                    "values": [
                      {
                        "name": "短裙",
                        "uid": 65,
                        "children": []
                      }
                    ],
                    "uid": 5
                  },
                  {
                    "name": "颜色分类",
                    "values": [
                      {
                        "name": "白色",
                        "uid": 71,
                        "children": []
                      },
                      {
                        "name": "灰色",
                        "uid": 76,
                        "children": []
                      }
                    ],
                    "uid": 6
                  },
                  {
                    "name": "裙边",
                    "values": [
                      {
                        "name": "齐边",
                        "uid": 5917,
                        "children": []
                      }
                    ],
                    "uid": 2115
                  },
                  {
                    "name": "廓形",
                    "values": [
                      {
                        "name": "A型",
                        "uid": 7424,
                        "children": []
                      }
                    ],
                    "uid": 2702
                  },
                  {
                    "name": "裙摆",
                    "values": [
                      {
                        "name": "竖褶裙",
                        "uid": 7487,
                        "children": []
                      }
                    ],
                    "uid": 2722
                  }
                ],
                "xmin": 0.3813,
                "ymin": 0.306,
                "uid": 3772525,
                "xmax": 0.6546
              }
            ],
            "uid": 2
          }
        ]
      },
      {
        "width": 750,
        "height": 789,
        "url": "http://pic.adkalava.com/img005/794/cc0895957808b262.jpg",
        "imgid": "100340601542049486",
        "objs": [
          {
            "name": "连衣裙",
            "bndBoxes": [
              {
                "ymax": 0.76531669695321,
                "attrs": [
                  {
                    "name": "领型",
                    "values": [
                      {
                        "name": "圆领",
                        "uid": 7587,
                        "children": []
                      }
                    ],
                    "uid": 2728
                  },
                  {
                    "name": "袖长",
                    "values": [
                      {
                        "name": "长袖",
                        "uid": 7532,
                        "children": []
                      }
                    ],
                    "uid": 2725
                  },
                  {
                    "name": "图案",
                    "values": [
                      {
                        "name": "格子",
                        "uid": 29,
                        "children": [
                          {
                            "name": "大小",
                            "uid": 30,
                            "children": [
                              {
                                "name": "大格子",
                                "uid": 31,
                                "children": []
                              }
                            ]
                          }
                        ]
                      }
                    ],
                    "uid": 3
                  },
                  {
                    "name": "裙长",
                    "values": [
                      {
                        "name": "中裙",
                        "uid": 66,
                        "children": []
                      }
                    ],
                    "uid": 5
                  },
                  {
                    "name": "颜色分类",
                    "values": [
                      {
                        "name": "灰色",
                        "uid": 76,
                        "children": []
                      }
                    ],
                    "uid": 6
                  },
                  {
                    "name": "面料",
                    "values": [
                      {
                        "name": "针织",
                        "uid": 5902,
                        "children": []
                      }
                    ],
                    "uid": 2114
                  },
                  {
                    "name": "裙边",
                    "values": [
                      {
                        "name": "齐边",
                        "uid": 5917,
                        "children": []
                      }
                    ],
                    "uid": 2115
                  },
                  {
                    "name": "廓形",
                    "values": [
                      {
                        "name": "H型",
                        "uid": 7425,
                        "children": []
                      }
                    ],
                    "uid": 2702
                  },
                  {
                    "name": "裙摆",
                    "values": [
                      {
                        "name": "常规",
                        "uid": 7483,
                        "children": []
                      }
                    ],
                    "uid": 2722
                  }
                ],
                "xmin": 0.3493,
                "ymin": 0.3004,
                "uid": 3772547,
                "xmax": 0.6653
              }
            ],
            "uid": 2
          }
        ]
      },
      {
        "width": 750,
        "height": 1271,
        "url": "http://pic.adkalava.com/img005/354/d21b2ac99babb322.jpg",
        "imgid": "10034175423989792050",
        "objs": [
          {
            "name": "连衣裙",
            "bndBoxes": [
              {
                "ymax": 0.93553933548394,
                "attrs": [
                  {
                    "name": "图案",
                    "values": [
                      {
                        "name": "条纹",
                        "uid": 18,
                        "children": [
                          {
                            "name": "轨迹",
                            "uid": 19,
                            "children": [
                              {
                                "name": "错误",
                                "uid": 5893,
                                "children": []
                              }
                            ]
                          }
                        ]
                      }
                    ],
                    "uid": 3
                  },
                  {
                    "name": "颜色分类",
                    "values": [
                      {
                        "name": "蓝色",
                        "uid": 74,
                        "children": []
                      }
                    ],
                    "uid": 6
                  }
                ],
                "xmin": 0.152,
                "ymin": 0.332,
                "uid": 3772586,
                "xmax": 0.9187
              }
            ],
            "uid": 2
          }
        ]
      },
      {
        "width": 750,
        "height": 841,
        "url": "http://pic.adkalava.com/img005/761/c4abc29db2099f89.jpg",
        "imgid": "10034238636653433186",
        "objs": [
          {
            "name": "连衣裙",
            "bndBoxes": [
              {
                "ymax": 0.88277485032532,
                "attrs": [
                  {
                    "name": "图案",
                    "values": [
                      {
                        "name": "条纹",
                        "uid": 18,
                        "children": [
                          {
                            "name": "轨迹",
                            "uid": 19,
                            "children": [
                              {
                                "name": "横条纹",
                                "uid": 20,
                                "children": []
                              }
                            ]
                          }
                        ]
                      }
                    ],
                    "uid": 3
                  },
                  {
                    "name": "颜色分类",
                    "values": [
                      {
                        "name": "灰色",
                        "uid": 76,
                        "children": []
                      },
                      {
                        "name": "蓝色",
                        "uid": 74,
                        "children": []
                      }
                    ],
                    "uid": 6
                  }
                ],
                "xmin": 0.16071308616033,
                "ymin": 0.27422710728894,
                "uid": 3772596,
                "xmax": 0.8513871324154
              },
              {
                "ymax": 0.5696,
                "attrs": [
                  {
                    "name": "图案",
                    "values": [
                      {
                        "name": "条纹",
                        "uid": 18,
                        "children": [
                          {
                            "name": "轨迹",
                            "uid": 19,
                            "children": [
                              {
                                "name": "横条纹",
                                "uid": 20,
                                "children": []
                              }
                            ]
                          }
                        ]
                      }
                    ],
                    "uid": 3
                  }
                ],
                "xmin": 0.2347,
                "ymin": 0.239,
                "uid": 3772598,
                "xmax": 0.7814
              }
            ],
            "uid": 2
          }
        ]
      },
      {
        "width": 750,
        "height": 1229,
        "url": "http://pic.adkalava.com/img005/478/d2f63e15ec876ce6.jpg",
        "imgid": "10034272410942004783",
        "objs": [
          {
            "name": "连衣裙",
            "bndBoxes": [
              {
                "ymax": 0.88635434614863,
                "attrs": [
                  {
                    "name": "图案",
                    "values": [
                      {
                        "name": "条纹",
                        "uid": 18,
                        "children": [
                          {
                            "name": "轨迹",
                            "uid": 19,
                            "children": [
                              {
                                "name": "竖条纹",
                                "uid": 21,
                                "children": []
                              }
                            ]
                          }
                        ]
                      }
                    ],
                    "uid": 3
                  },
                  {
                    "name": "颜色分类",
                    "values": [
                      {
                        "name": "蓝色",
                        "uid": 74,
                        "children": []
                      }
                    ],
                    "uid": 6
                  }
                ],
                "xmin": 0.1227,
                "ymin": 0.3531,
                "uid": 3772617,
                "xmax": 0.896
              }
            ],
            "uid": 2
          }
        ]
      },
      {
        "width": 750,
        "height": 665,
        "url": "http://pic.adkalava.com/img005/162/c550dbaadb68945a.jpg",
        "imgid": "10034295182571682693",
        "objs": [
          {
            "name": "连衣裙",
            "bndBoxes": [
              {
                "ymax": 0.89440301070336,
                "attrs": [
                  {
                    "name": "图案",
                    "values": [
                      {
                        "name": "条纹",
                        "uid": 18,
                        "children": [
                          {
                            "name": "轨迹",
                            "uid": 19,
                            "children": [
                              {
                                "name": "横条纹",
                                "uid": 20,
                                "children": []
                              }
                            ]
                          }
                        ]
                      }
                    ],
                    "uid": 3
                  },
                  {
                    "name": "颜色分类",
                    "values": [
                      {
                        "name": "灰色",
                        "uid": 76,
                        "children": []
                      },
                      {
                        "name": "蓝色",
                        "uid": 74,
                        "children": []
                      }
                    ],
                    "uid": 6
                  }
                ],
                "xmin": 0.62050936236302,
                "ymin": 0.30135948150435,
                "uid": 3772625,
                "xmax": 0.9117152250622
              },
              {
                "ymax": 0.5834,
                "attrs": [
                  {
                    "name": "图案",
                    "values": [
                      {
                        "name": "条纹",
                        "uid": 18,
                        "children": [
                          {
                            "name": "轨迹",
                            "uid": 19,
                            "children": [
                              {
                                "name": "横条纹",
                                "uid": 20,
                                "children": []
                              }
                            ]
                          }
                        ]
                      }
                    ],
                    "uid": 3
                  }
                ],
                "xmin": 0.0827,
                "ymin": 0.2887,
                "uid": 3772628,
                "xmax": 0.4094
              },
              {
                "ymax": 0.89633736247336,
                "attrs": [
                  {
                    "name": "颜色分类",
                    "values": [
                      {
                        "name": "灰色",
                        "uid": 76,
                        "children": []
                      },
                      {
                        "name": "蓝色",
                        "uid": 74,
                        "children": []
                      }
                    ],
                    "uid": 6
                  }
                ],
                "xmin": 0.051780923747562,
                "ymin": 0.28779696935631,
                "uid": 3772629,
                "xmax": 0.41274337974716
              }
            ],
            "uid": 2
          }
        ]
      },
      {
        "width": 750,
        "height": 803,
        "url": "http://pic.adkalava.com/img005/108/d8eb500e801df77c.jpg",
        "imgid": "10034504997889585655",
        "objs": [
          {
            "name": "连衣裙",
            "bndBoxes": [
              {
                "ymax": 0.89598690096899,
                "attrs": [
                  {
                    "name": "领型",
                    "values": [
                      {
                        "name": "圆领",
                        "uid": 7587,
                        "children": []
                      }
                    ],
                    "uid": 2728
                  },
                  {
                    "name": "袖长",
                    "values": [
                      {
                        "name": "长袖",
                        "uid": 7532,
                        "children": []
                      }
                    ],
                    "uid": 2725
                  },
                  {
                    "name": "图案",
                    "values": [
                      {
                        "name": "格子",
                        "uid": 29,
                        "children": [
                          {
                            "name": "大小",
                            "uid": 30,
                            "children": [
                              {
                                "name": "错误",
                                "uid": 5890,
                                "children": []
                              }
                            ]
                          }
                        ]
                      }
                    ],
                    "uid": 3
                  },
                  {
                    "name": "裙长",
                    "values": [
                      {
                        "name": "中裙",
                        "uid": 66,
                        "children": []
                      }
                    ],
                    "uid": 5
                  },
                  {
                    "name": "颜色分类",
                    "values": [
                      {
                        "name": "黑色",
                        "uid": 70,
                        "children": []
                      }
                    ],
                    "uid": 6
                  },
                  {
                    "name": "面料",
                    "values": [
                      {
                        "name": "针织",
                        "uid": 5902,
                        "children": []
                      }
                    ],
                    "uid": 2114
                  },
                  {
                    "name": "裙边",
                    "values": [
                      {
                        "name": "齐边",
                        "uid": 5917,
                        "children": []
                      }
                    ],
                    "uid": 2115
                  },
                  {
                    "name": "廓形",
                    "values": [
                      {
                        "name": "H型",
                        "uid": 7425,
                        "children": []
                      }
                    ],
                    "uid": 2702
                  },
                  {
                    "name": "裙摆",
                    "values": [
                      {
                        "name": "常规",
                        "uid": 7483,
                        "children": []
                      }
                    ],
                    "uid": 2722
                  }
                ],
                "xmin": 0.3387,
                "ymin": 0.2989,
                "uid": 3772677,
                "xmax": 0.7387
              }
            ],
            "uid": 2
          }
        ]
      },
      {
        "width": 750,
        "height": 798,
        "url": "http://pic.adkalava.com/img005/767/e332b210b24c674f.jpg",
        "imgid": "10034609582419340852",
        "objs": [
          {
            "name": "连衣裙",
            "bndBoxes": [
              {
                "ymax": 0.93368622682679,
                "attrs": [
                  {
                    "name": "领型",
                    "values": [
                      {
                        "name": "圆领",
                        "uid": 7587,
                        "children": []
                      }
                    ],
                    "uid": 2728
                  },
                  {
                    "name": "袖长",
                    "values": [
                      {
                        "name": "长袖",
                        "uid": 7532,
                        "children": []
                      }
                    ],
                    "uid": 2725
                  },
                  {
                    "name": "图案",
                    "values": [
                      {
                        "name": "格子",
                        "uid": 29,
                        "children": []
                      }
                    ],
                    "uid": 3
                  },
                  {
                    "name": "裙长",
                    "values": [
                      {
                        "name": "中裙",
                        "uid": 66,
                        "children": []
                      }
                    ],
                    "uid": 5
                  },
                  {
                    "name": "颜色分类",
                    "values": [
                      {
                        "name": "灰色",
                        "uid": 76,
                        "children": []
                      }
                    ],
                    "uid": 6
                  },
                  {
                    "name": "面料",
                    "values": [
                      {
                        "name": "针织",
                        "uid": 5902,
                        "children": []
                      }
                    ],
                    "uid": 2114
                  },
                  {
                    "name": "裙边",
                    "values": [
                      {
                        "name": "齐边",
                        "uid": 5917,
                        "children": []
                      }
                    ],
                    "uid": 2115
                  },
                  {
                    "name": "廓形",
                    "values": [
                      {
                        "name": "H型",
                        "uid": 7425,
                        "children": []
                      }
                    ],
                    "uid": 2702
                  },
                  {
                    "name": "裙摆",
                    "values": [
                      {
                        "name": "常规",
                        "uid": 7483,
                        "children": []
                      }
                    ],
                    "uid": 2722
                  }
                ],
                "xmin": 0.2733,
                "ymin": 0.3108,
                "uid": 3772733,
                "xmax": 0.6533
              },
              {
                "ymax": 0.6003,
                "attrs": [
                  {
                    "name": "领型",
                    "values": [
                      {
                        "name": "圆领",
                        "uid": 7587,
                        "children": []
                      }
                    ],
                    "uid": 2728
                  },
                  {
                    "name": "袖长",
                    "values": [
                      {
                        "name": "长袖",
                        "uid": 7532,
                        "children": []
                      }
                    ],
                    "uid": 2725
                  },
                  {
                    "name": "图案",
                    "values": [
                      {
                        "name": "格子",
                        "uid": 29,
                        "children": [
                          {
                            "name": "大小",
                            "uid": 30,
                            "children": [
                              {
                                "name": "错误",
                                "uid": 5890,
                                "children": []
                              }
                            ]
                          }
                        ]
                      }
                    ],
                    "uid": 3
                  },
                  {
                    "name": "裙长",
                    "values": [
                      {
                        "name": "中裙",
                        "uid": 66,
                        "children": []
                      }
                    ],
                    "uid": 5
                  },
                  {
                    "name": "面料",
                    "values": [
                      {
                        "name": "针织",
                        "uid": 5902,
                        "children": []
                      }
                    ],
                    "uid": 2114
                  },
                  {
                    "name": "裙边",
                    "values": [
                      {
                        "name": "齐边",
                        "uid": 5917,
                        "children": []
                      }
                    ],
                    "uid": 2115
                  },
                  {
                    "name": "廓形",
                    "values": [
                      {
                        "name": "H型",
                        "uid": 7425,
                        "children": []
                      }
                    ],
                    "uid": 2702
                  },
                  {
                    "name": "裙摆",
                    "values": [
                      {
                        "name": "常规",
                        "uid": 7483,
                        "children": []
                      }
                    ],
                    "uid": 2722
                  }
                ],
                "xmin": 0.2733,
                "ymin": 0.3108,
                "uid": 3772734,
                "xmax": 0.6533
              }
            ],
            "uid": 2
          }
        ]
      }
    ]
  },
  objs: [
    {
      name: '连衣裙',
      uid: 42341,
      geometry: "bndbox",
      bndBoxes: [
        {
          uid: 2,
          editable: false,
          xmin: 0.219,
          ymin: 0.1243,
          xmax: 0.904,
          ymax: 0.599,
          attrs: [
            {
              uid: 2115,
              name: "裙型",
              values: [
                {
                  name: "E",
                  uid: 4
                }
              ]
            }
          ]
        }
      ],
      attrs: [
        {
          unique: true,
          uid: 2115,
          type: 2,
          display: true,
          name: '裙型',
          required: true,
          options: [
            { name: 'A型裙', uid: 0 },
            { name: 'B型裙', uid: 1 },
            { name: 'C型裙', uid: 2 },
            { name: 'D型裙', uid: 3 },
            { name: 'E型裙', uid: 4 },
            { name: 'F型裙', uid: 5 },
            { name: 'G型裙', uid: 6 },
            { name: 'H型裙', uid: 7 }
          ]
        },
        {
          uid: 1,
          type: 1,
          name: '颜色',
          display: true,
          options: [
            { name: '红', uid: 0 },
            { name: '黄', uid: 1 },
            { name: '蓝', uid: 2 }
          ]
        }
      ]
    },
    {
      name: '新裙子',
      uid: 42342,
      geometry: "bndbox",
      attrs: [
        {
          uid: 0,
          name: '属性1',
          options: [
            { name: 'A', uid: 0 },
            { name: 'B', uid: 1 },
            { name: 'C', uid: 2 },
            { name: 'D', uid: 3 },
            { name: 'E', uid: 4 },
            { name: 'F', uid: 5 },
            { name: 'G', uid: 6 },
            { name: 'H', uid: 7 }
          ]
        }
      ]
    }
  ]
};

module.exports = image;
