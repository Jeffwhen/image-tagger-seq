import React from 'react';
import ReactDOM from 'react-dom';
import * as Bootstrap from 'react-bootstrap';
import * as Utils from './utils.js';
import mousetrap from 'mousetrap';
const querystring = require('querystring');

require('./image-tagger.css');

import { postJSONPromise } from './utils.js';
import ControlPannel from './control-pannel.js';
import PictureEdit from './canvas.js';
import Pagination from './pagination.js';
import Sequence from './sequence.js';
import { SeqPagination } from './sequence.js';
import AttrsBox from './attrs-box.js';
import ImageDataValidate from './validate.js';

window.imageTaggerSetup = initialize;

function initialize(id, url, taskId, tools, callback) {
  if (!id) {
    throw new Error('Container id needed.');
    return;
  }
  if (!url || typeof taskId !== 'number') {
    throw new Error('ImageTaggerWarning: image URL or taskId is empty.');
    return;
  }
  var seqId = Math.floor(window.location.hash.slice(1));
  if (Number.isNaN(seqId))
    seqId = 0;
  var wrapper = document.getElementById(id);
  var rootComponent = ReactDOM.render(
    <ImageTagger
      taskId={taskId}
      url={ url }
      seqId={ seqId }
      tools={ tools }
    />,
    wrapper
  );
}

class ConfirmBox extends React.Component {
  constructor(props) {
    // `props.onConfirm`
    // `props.onCancel`
    // `props.body`
    // `props.title`
    super(props);
    this.state = {
      showModal: false
    };
  }
  show() {
    this.state.showModal = true;
    this.forceUpdate();
  }
  _handleConfirm() {
    this.state.showModal = false;
    this.forceUpdate();
    if (this.props.onConfirm)
      this.props.onConfirm();
  }
  _handleCancel() {
    this.state.showModal = false;
    this.forceUpdate();
    if (this.props.onCancel)
      this.props.onCancel();
  }
  _createCancelButton() {
    return this.props.hideCancel ? [] : (
      <Bootstrap.Button onClick={ this._handleCancel.bind(this)}
      >取消</Bootstrap.Button>
    )
  }
  render() {
    return (
      <Bootstrap.Modal show={ this.state.showModal } bsSize="small">
      <Bootstrap.Modal.Header>
      <Bootstrap.Modal.Title>{ this.props.title }</Bootstrap.Modal.Title>
      </Bootstrap.Modal.Header>
      <Bootstrap.Modal.Body>
      { this.props.body }
      </Bootstrap.Modal.Body>
      <Bootstrap.Modal.Footer>
      { this._createCancelButton() }
      <Bootstrap.Button
      onClick={ this._handleConfirm.bind(this) }
      bsStyle="primary">确定</Bootstrap.Button>
      </Bootstrap.Modal.Footer>
      </Bootstrap.Modal>
    );
  }
}

class ImageTagger extends React.Component {
  constructor(props) {
    super(props);
    // `props.url`
    // `props.taskId`
    // `props.seqId`
    // `props.tools`
    this._url = this.props.url;
    this._taskId = this.props.taskId;
    this._seqId = this.props.seqId || 0;
    this._data = null;
    this._mutateStatus = false;
    this.state = {};

    // Components will be bind after mount.
    this._pagination = null;
    this._canvas = null;
    this._attrsBox = null;
    this._config = null;

    this._confirmDialog = this._dialog;
    this._alert = this._errorDialog;

    // Key binding.
    this.suspending = false;
    mousetrap.bind(
      'enter',
      this._handleEnterKeydown.bind(this),
      'keyup'
    );
    mousetrap.bind(
      'enter',
      this._preventDefault.bind(this)
    );
    mousetrap.bind(
      'left',
      this._handleLeftKeydown.bind(this),
      'keyup'
    );
    mousetrap.bind(
      'right',
      this._handleRightKeydown.bind(this),
      'keyup'
    );
  }
  _preventDefault(event) {
    if (event.target.tagName == 'INPUT') {
      event.target.blur();
      return;
    }
    event.preventDefault();
  }
  _handleLeftKeydown() {
    if (this.suspending)
      return;
    this._handlePageIndicatorClicked(-1);
  }
  _handleRightKeydown() {
    if (this.suspending)
      return;
    this._handlePageIndicatorClicked(1);
  }
  _handleEnterKeydown(event) {
    if (event.target.tagName == 'INPUT') {
      event.target.blur();
      return;
    }
    console.log('enter');
    if (this.suspending)
      return;
    this._handleSubmit();
  }
  get URLWithQuery() {
    var query = querystring.stringify({
      taskid: this._taskId,
      seqid: this._seqId
    });
    return this._url + '?' + query;
  }
  _dialog(title, body, confirmCallback, cancelCallback, hideCancel) {
    this.suspending = true;
    let confirm = () => {
      this.suspending = false;
      if (confirmCallback)
        confirmCallback();
    };
    let cancel = () => {
      this.suspending = false;
      if (cancelCallback)
        cancelCallback();
    };
    var box = ReactDOM.render(
      <ConfirmBox
        body={ body }
        title={ title }
        onConfirm={ confirm }
        onCancel={ cancel }
        hideCancel={ hideCancel }
      />,
      document.getElementById('image-tagger-modal')
    );
    box.show();
  }
  _errorDialog(title, body) {
    this._dialog(title, body, null, null, true);
  }
  _updateCurrentSlice(callback) {
    this._canvas.cropMode.clearCanvas();
    Utils.requestForImage(this._url, this._taskId, this._seqId, (
      err, data
    ) => {
      if (err || !data || data.errcode !== 0) {
        var err = err ? JSON.stringify(err) : '';
        var msg = `${data.errmsg}\n${err}`;
        this._errorDialog('获取图片失败', msg);
        return;
      }
      if (!ImageDataValidate(data)) {
        this._errorDialog('获取图片失败', '服务器返回数据格式错误');
        return;
      }
      this._data = data;
      this.state.sequence = data.sequence;
      this.state.imgid = data.imgid;
      // Reference info.
      this.state.referURL = data.src;
      this.state.description = data.description;

      // Set pagination to right place.
      this._pagination.length = data.maxseqid + 1;
      this._pagination.activePage = this._seqId + 1;

      // Update image.
      this._canvas.cropMode.updateImage(data.url, () => {
        // Restore rects.
        this._canvas.cropMode.clearCanvas();
        data.objs.forEach(object => {
          this._canvas.cropMode.name = object.name;
          if (!object.bndBoxes)
            object.rects = [];
          else {
            object.rects = object.bndBoxes.map((box, index) => {
              if (!box.attrs)
                box.attrs = [];
              for (var i in box.attrs) {
                var attr = box.attrs[i];
                var objAttr = object.attrs.filter(objectAttr => {
                  return objectAttr.uid === attr.uid;
                })[0];
                if (objAttr && objAttr.type !== undefined) {
                  attr.type = objAttr.type;
                  console.assert(0 < attr.type && attr.type < 10);
                  attr.uid = Number(String(attr.uid) + attr.type);
                } else if (objAttr) {
                  attr.uid = attr.uid * 10;
                }
                attr.display = objAttr && objAttr.display;
                if (attr.display === undefined)
                  attr.display = true;
                attr.unique = objAttr && objAttr.unique || false;
              }
              box.description = this._getDescriptionFromAttrList(box.attrs);
              if (box.uid) {
                box.uid = box.uid;
                box.upstream = true;
              }
              box.objectId = object.uid;
              box.top = box.ymin;
              box.left = box.xmin;
              box.height = box.ymax - box.ymin;
              box.width = box.xmax - box.xmin;
              delete box.xmax;
              delete box.xmin;
              delete box.ymax;
              delete box.ymin;
              return box;
            });
          }
          this._canvas.cropMode.addCrops(
            object.rects,
            ['boxId']
          );
          object.attrs.forEach(objAttr => {
            if (objAttr.display === undefined)
              objAttr.display = true;
            if (objAttr.required === undefined)
              objAttr.required = true;
            objAttr.uid = Number(
              String(objAttr.uid) + Number(objAttr.type || 0)
            );
          });
          delete object.bndBoxes;
        });

        // Update objects and their attributes.
        this._attrsBox.update(data.objs);
      });
      this._canvas.cropMode.refresh();
      this.forceUpdate();
      this._mutateStatus = false;
      if (callback)
        callback();
    });
  }
  // Event handler for `controlPannel`
  _handleCommand(options) {
    // TODO
    if (options.id === 'select-tool' &&
        options.selected.value === 'select') {
      this._canvas.cropMode.selectMode();
    } else if (options.id === 'tag-tool' &&
               options.selected.value === 'drag-tag') {
      this._canvas.changeMode('drag');
      this._canvas.cropMode.tagMode();
      this._canvas.cropMode.stroke = this._config.getColor();
    } else if (options.id === 'tag-tool' &&
               options.selected.value === 'dot-tag') {
      this._canvas.changeMode('click');
      this._canvas.cropMode.tagMode();
      this._canvas.cropMode.stroke = this._config.getColor();
    } else if (options.id === 'select-tool' &&
               options.selected.value === 'remove') {
      this._canvas.cropMode.removeCrops();
      this._canvas.cropMode.refresh();
    } else if (options.id === 'select-tool' &&
               options.selected.value === 'select-all') {
      this._canvas.cropMode.selectMode();
      this._canvas.cropMode.setActiveCrops(
        this._canvas.cropMode.getCrops().map(crop => {
          return crop.uid;
        })
      );
      this._canvas.cropMode.refresh();
    } else if (options.id === 'color') {
      this._canvas.cropMode.stroke = options.selected.value;
      this._canvas.cropMode.refresh();
    }
  }
  // Event handler for `canvas`
  // TODO group
  _handleCropSelected(crop) {
    if (typeof crop.objectId !== 'number') {
      crop.objectId = this._attrsBox.getSelectedObject().uid;
    }
    else {
      // This expression DOES NOT triggle selectedObject change event.
      this._attrsBox.setSelectedObject(crop.objectId);
    }
    if (!(crop.attrs instanceof Array)) {
      crop.attrs = [];
    }
    this._attrsBox.setSelectedAttrs(crop.attrs);
  }
  // Event handler for `attrsBox`.
  _handleSelectedObjectChange(selectedObject) {
    this._canvas.cropMode.name = selectedObject.name;
    this._canvas.cropMode.objectId = selectedObject.uid;
    this._canvas.cropMode.refresh();
  }
  _getDescriptionFromAttrList(attrs) {
    var description = '';
    for (var i in attrs) {
      var attr = attrs[i];
      if (!attr.display)
        continue;
      attr.values = attr.values || []
      attr.values.forEach(value => {
        description += value.name + '&';
      });
      description = description.slice(0, -1);
      description += ' ';
    }
    description = description.slice(0, -1);
    return description;
  }
  // Event handler for `attrsBox`.
  _handleAttrChange(attrs) {
    var description = this._getDescriptionFromAttrList(attrs);
    this._canvas.cropMode.description = description;
    this._canvas.cropMode.refresh();
  }
  _handlePageIndicatorClicked(step) {
    if (!this._hasChanged()) {
      this._pagination.incKey(step);
      return;
    }
    var confirmCallback = () => {
      this._pagination.incKey(step);
    };
    this._confirmDialog(
      '放弃编辑?',
      '如需保存请点击提交按钮',
      confirmCallback
    );
  }
  _handleBeforePageChange() {
    return true;
  }
  // Event handler for `pagination`
  _handlePageChange(eventKey, callback) {
    this._seqId = eventKey - 1;
    window.location.hash = '#' + this._seqId;
    this._updateCurrentSlice(callback);
  }
  _handleSubmit(callback) {
    this._submit((data) => {
      if (data && data.errcode)
        this._alert(
          '消息',
          data.errmsg || ''
        );
      else if (data) {
        this._pagination.length = data.maxseqid + 1;
        this._pagination.incKey(1);
      }
      if (callback)
        callback();
    });
  }
  _handleSkip(callback) {
    this._skip((data) => {
      if (data && data.errcode)
        this._alert(
          '消息',
          data.errmsg || ''
        );
      else if (data) {
        this._pagination.length = data.maxseqid + 1;
        this._pagination.incKey(1);
      }
      if (callback)
        callback();
    });
  }
  _rectEqual(a, b) {
    if (a === b) return true;
    if (!a || !b) return false;
    var isEqual = true;
    if (JSON.stringify(a.attrs) !== JSON.stringify(b.attrs))
      isEqual = false;
    var samePos = a.top === b.top && a.left === b.left &&
                  a.width === b.width && a.height === b.height;
    if (!samePos)
      isEqual = false;
    return isEqual;
  }
  // Check if rects has chenged.
  _hasChanged() {
    if (this._mutateStatus)
      return true;
    var crops = this._canvas.cropMode.getCrops();
    var origRectNum = 0;
    this._data.objs.forEach(object => {
      origRectNum += object.rects.length;
    });
    if (crops.length !== origRectNum) {
      this._mutateStatus = true;
      return true;
    }
    var hasChanged = false;
    for (var i in crops) {
      var crop = crops[i];
      var object = this._data.objs.find((object) => {
        return object.uid === crop.objectId;
      });
      if (!object) return;
      var rect = object.rects.find((rect) => {
        return crop.uid === rect.uid;
      });
      if (!rect || !this._rectEqual(rect, crop)) {
        hasChanged = true;
        break;
      }
    }
    this._mutateStatus = hasChanged;
    return hasChanged;
  }
  seqIndex(index) {
    var postData = {
      seqid: index,
      imgid: this._data.imgid,
      unitid: this._data.unitid,
      url: this._data.url,
      operation: 'seq-index'
    };
    postJSONPromise(this.URLWithQuery, postData).then(data => {
      this._pagination._handleSelect(data.seqid + 1);
    });
  }
  _skip(callback) {
    var postData = {
      imgid: this._data.imgid,
      unitid: this._data.unitid,
      url: this._data.url,
      operation: 'skip'
    };
    postJSONPromise(this.URLWithQuery, postData).then(data => {
      this.hasChanged = false;
      if (callback)
        callback(data);
    });
  }
  _submit(callback) {
    this._data.objs.map(object => {
      object.rects = [];
    });
    var lackAttr = false;
    this._canvas.cropMode.getCrops().forEach(rect => {
      var object = this._data.objs.find(object => {
        return object.uid === rect.objectId;
      });
      object.attrs.forEach(attr => {
        if (!attr.required)
          return;
        var rectAttrs = rect.attrs || [];
        var rectHasAttr = rectAttrs.filter(rectAttr => {
          return rectAttr.uid === attr.uid;
        });
        if (!rectHasAttr.length)
          lackAttr = true;
      });
      try {
        rect.attrs.forEach(attr => {
          // To compatible with type in order to handle
          // embedded attribute.
          let attrId = Math.floor(attr.uid / 10);
          attr.uid = attrId;
          if (attr.required && !attr.values.length)
            lackAttr = true;
        });
        object.rects.push(rect);
      } catch (error) {
        console.error('ImageTaggerError: rect\'s objectId not valid!');
        throw error;
      }
    }); // `this._data` is updated now.
    var notEmpty = false;
    this._data.objs.forEach(object => {
      object.bndBoxes = object.rects.map(rect => {
        notEmpty = true;
        let box = {
          xmin: rect.left / this._canvas.cropMode.canvasWidth,
          ymin: rect.top / this._canvas.cropMode.canvasHeight,
          xmax: (rect.left + rect.width) / this._canvas.cropMode.canvasWidth,
          ymax: (rect.top + rect.height) / this._canvas.cropMode.canvasHeight,
          attrs: rect.attrs,
          stroke: rect.stroke
        };
        if (rect.upstream) {
          box.uid = rect.uid;
        }
        return box;
      });
    });
    var postData;
    if (notEmpty) {
      postData = {
        unitid: this._data.unitid,
        url: this._data.url,
        imgid: this._data.imgid,
        operation: 'alter',
        objs: this._data.objs.map(object => {
          return {
            name: object.name,
            uid: object.uid,
            geometry: object.geometry,
            bndBoxes: object.bndBoxes
          };
        }).filter(object => {
          return object.bndBoxes.length;
        })
      };
    } else {
      postData = {
        unitid: this._data.unitid,
        url: this._data.url,
        imgid: this._data.imgid,
        operation: 'skip',
      };
    }
    // Some required attributes are not provided.
    if (lackAttr) {
      this._alert(
        '必须属性未标注!',
        '请检查'
      );
      if (callback)
        callback();
    } else {
      postJSONPromise(this.URLWithQuery, postData).then(data => {
        this.hasChanged = false;
        if (callback)
          callback(data);
      });
    }
  }
  componentDidMount() {
    // Components binding.
    this._pagination = this.refs.pagination;
    this._canvas = this.refs.canvas;
    this._attrsBox = this.refs.attrsBox;
    this._config = this.refs.config;

    this._canvas.cropMode.stroke = this._config.getColor();
    this._config.triggleCommand();
    this._updateCurrentSlice();
  }
  render() {
    if (window.innerHeight < 900) {
      var imgStyle = {
        height: '400px'
      };
      var copStyle = {
        top: '400px'
      };
    }
    return (
      <div id="image-tagger">
        <PictureEdit
          ref="canvas"
          onCropSelected={ this._handleCropSelected.bind(this) }
          onPageInc={
            (step) => {
              this._pagination.incKey(step);
            }
                    }
          style={ imgStyle }
        />
        <div className="image-tagger-side-div">
          <ControlPannel
            mode={ this.props.tools }
            onCommand={ this._handleCommand.bind(this) }
            ref="config"
          />
          <AttrsBox
            ref="attrsBox"
            onAttrChange={ this._handleAttrChange.bind(this) }
            onSelectedObjectChange={ this._handleSelectedObjectChange.bind(this) }
          />
          <p className="image-tagger-reference-info">{ this.state.description }
            { this.state.referURL ?
              <a target="_blank" href={ this.state.referURL }> 图片源地址</a> :
              ''
            }
          </p>
        </div>
        <div className="image-tagger-image-cop" style={ copStyle }>
          <Pagination
            onChange={ this._handlePageChange.bind(this) }
            beforeChange={ this._handleBeforePageChange.bind(this) }
            onIndicatorClicked={ this._handlePageIndicatorClicked.bind(this) }
            onSubmit={ this._handleSubmit.bind(this) }
            onSkip={ this._handleSkip.bind(this) }
            ref="pagination"
          />
        </div>
        <Sequence
          sequence={ this.state.sequence }
          current={ this.state.imgid }
          seqIndex={ this.seqIndex.bind(this) }
        />
        <div id="image-tagger-modal"></div>
      </div>
    )
  }
}
