import $ from 'jquery';

//********************************************************************
// extend a with b
//********************************************************************
function extendDict(a, b) {
  var result = {};
  for (var prop in a) {
    result[prop] = a[prop];
  }
  for (var prop in b) {
    if (!result.hasOwnProperty(prop))
      result[prop] = b[prop];
  }
  return result;
}

function max(a, b) {
  if (b > a)
    return b;
  return a;
}

function min(a, b) {
  if (b < a)
    return b;
  return a;
}

function requestForImage(url, taskId, seqId, callback) {
  $.ajax({
    url: url,
    dataType: 'json',
    timeout: 2000,
    data: {
      taskid: taskId + '',
      seqid: seqId + ''
    },
    success: function (data) {
      if (callback)
        callback(null, data);
    },
    error: function (xhr, status, err) {
      console.error('ImageTaggerError: requestForImage, ', err);
      if (callback)
        callback(err);
      else
        throw err;
    }
  });
}

//********************************************************************
// postJSONPromise
// @param url
// @param json data
//********************************************************************
function postJSONPromise (url, jsonData) {
    var callback = function (resolve, reject) {
        var xhr;
        var stripData = function () {
            try {
                var data = JSON.parse(xhr.responseText);
            } catch (e) {
                reject(e);
            }
            resolve(data);
        };

        if (typeof XDomainRequest !== 'undefined') {
            xhr = new XDomainRequest();
            xhr.onload = stripData;
            xhr.onerror = function () {
                reject(new Error(xhr.statusText));
            };
        } else {
            xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function () {
                if (xhr.readyState !== 4)
                    return;
                if (xhr.status === 200) {
                    stripData();
                } else {
                    reject(new Error(xhr.statusText));
                }
            };
        }
        xhr.open("POST", url);
        xhr.setRequestHeader("Content-Type", "application/json");
        if (jsonData) {
            var data = typeof jsonData === "string" ? jsonData :
                JSON.stringify(jsonData);
            xhr.send(data);
        } else {
            xhr.send();
        }
    };
    return new Promise(callback);
}

export { extendDict, max, min, requestForImage, postJSONPromise };
